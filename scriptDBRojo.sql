-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.1.30-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win32
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para rojo_carmesi
CREATE DATABASE IF NOT EXISTS `rojo_carmesi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rojo_carmesi`;

-- Volcando estructura para tabla rojo_carmesi.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `appaterno` varchar(40) DEFAULT NULL,
  `apmaterno` varchar(40) DEFAULT NULL,
  `celular` varchar(50) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `telCasa` int(10) DEFAULT NULL,
  `activo` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `contrasena` varchar(255) DEFAULT NULL,
  `perfil` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.domicilio
CREATE TABLE IF NOT EXISTS `domicilio` (
  `idDomicilio` int(11) NOT NULL AUTO_INCREMENT,
  `id` int(11) DEFAULT NULL,
  `calle` varchar(70) DEFAULT NULL,
  `cruzamientos` varchar(100) DEFAULT NULL,
  `numero_ext` int(11) DEFAULT NULL,
  `numero_int` int(11) DEFAULT NULL,
  `colonia_fracc` varchar(70) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `cp` int(5) DEFAULT NULL,
  PRIMARY KEY (`idDomicilio`),
  KEY `idCliente_Domicilio_idx` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.domicilio_sinregistro
CREATE TABLE IF NOT EXISTS `domicilio_sinregistro` (
  `iddomicilio` int(11) NOT NULL AUTO_INCREMENT,
  `idpedido` int(11) DEFAULT NULL,
  `nombre_sin` varchar(250) DEFAULT NULL,
  `calle` varchar(100) DEFAULT NULL,
  `cruzamientos` varchar(150) DEFAULT NULL,
  `numero_ext` int(11) DEFAULT NULL,
  `numero_int` int(11) DEFAULT NULL,
  `colonia_fracc` varchar(150) DEFAULT NULL,
  `ciudad` varchar(150) DEFAULT NULL,
  `estado` varchar(150) DEFAULT NULL,
  `cp` int(5) DEFAULT NULL,
  PRIMARY KEY (`iddomicilio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.fletes
CREATE TABLE IF NOT EXISTS `fletes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_flete` varchar(255) NOT NULL DEFAULT '0',
  `costo` float NOT NULL DEFAULT '0',
  `ruta_logo` varchar(155) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.imagenes
CREATE TABLE IF NOT EXISTS `imagenes` (
  `idImagen` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `rutaImagen` varchar(255) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idImagen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.pedido
CREATE TABLE IF NOT EXISTS `pedido` (
  `idpedido` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) DEFAULT NULL,
  `costoPedido` float(11,2) DEFAULT NULL,
  `estatusPedido` varchar(25) DEFAULT NULL,
  `activo` tinyint(4) DEFAULT NULL,
  `fecha_levantada` datetime DEFAULT NULL,
  `fecha_update` datetime DEFAULT NULL,
  PRIMARY KEY (`idpedido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.productos
CREATE TABLE IF NOT EXISTS `productos` (
  `idProducto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(70) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `codigoInterno` varchar(25) DEFAULT NULL,
  `precio` float(12,2) DEFAULT NULL,
  `activo` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seccion` varchar(50) DEFAULT NULL,
  `txCh` tinyint(1) DEFAULT '0',
  `tCh` tinyint(1) DEFAULT '0',
  `tM` tinyint(1) DEFAULT '0',
  `tG` tinyint(1) DEFAULT '0',
  `txG` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.rel_pedido_producto
CREATE TABLE IF NOT EXISTS `rel_pedido_producto` (
  `idPedidoProd` int(11) NOT NULL AUTO_INCREMENT,
  `idPedido` int(11) DEFAULT NULL,
  `idProducto` int(11) DEFAULT NULL,
  `talla` varchar(10) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPedidoProd`),
  KEY `pedido_idx` (`idPedido`),
  KEY `producto_idx` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.rel_producto_imagen
CREATE TABLE IF NOT EXISTS `rel_producto_imagen` (
  `idProductoImagen` int(11) NOT NULL AUTO_INCREMENT,
  `idProducto` int(11) DEFAULT NULL,
  `IdImagen` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProductoImagen`),
  KEY `producto_idx` (`idProducto`),
  KEY `imagen_idx` (`IdImagen`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.role_user
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.tmpventas
CREATE TABLE IF NOT EXISTS `tmpventas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_c` varchar(100) DEFAULT NULL,
  `correo_c` varchar(50) DEFAULT NULL,
  `celular_c` varchar(10) DEFAULT NULL,
  `calle_c` varchar(150) DEFAULT NULL,
  `nointerior_c` int(8) DEFAULT NULL,
  `noexterior_c` int(8) DEFAULT NULL,
  `cruzamiento_c` varchar(80) DEFAULT NULL,
  `colonia_c` varchar(80) DEFAULT NULL,
  `ciudad_c` varchar(80) DEFAULT NULL,
  `estado_c` varchar(80) DEFAULT NULL,
  `cp_c` int(5) DEFAULT NULL,
  `tota_neto` float DEFAULT NULL,
  `costo_envio` float DEFAULT NULL,
  `img_prod` varchar(255) NOT NULL,
  `acompletado_r` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla rojo_carmesi.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appaterno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apmaterno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `celular` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_user` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT '0',
  `confirmation_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
