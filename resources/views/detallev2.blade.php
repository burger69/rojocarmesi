@extends('base')


@section('content')	


<div class="single_product">
		<div class="container">
			<div class="row">

				<!-- Images -->
				<!--<li data-image="images/single_4.jpg"><img src="../images/single_4.jpg" alt=""></li>
					<li data-image="images/single_2.jpg"><img src="../images/single_2.jpg" alt=""></li>
					<li data-image="images/single_3.jpg"><img src="../images/single_3.jpg" alt=""></li>-->
				<div id="test"></div>
				<div class="col-lg-2 order-lg-1 order-2">
					<ul class="image_list">
						@foreach($info_producto as $info_individual)													
							@if($info_individual->portada_img != 1)	
								<li data-image="images/single_4.jpg">
									<img class="imgss" src="{{asset($info_individual->ruta_imagen)}}">
								</li>	
							@endif
						@endforeach
					</ul>
				</div>
				

				@foreach($info_producto as $info_individual)													
					@if($info_individual->portada_img == 1)	
						<div class="col-lg-5 order-lg-2 order-1">
							<div class="image_selected">
								<a href="{{asset($info_individual->ruta_imagen)}}" id="zom" title="{{$info_individual->producto_nombre}}" data-subtitle="Vista Previa" data-fancybox="preview" data-height="1000" data-caption="<strong>{{$info_individual->descripcion}}</strong>">
										<img id="portada" data-zoom-image="{{asset($info_individual->ruta_imagen)}}}}" class="zoom_01" src="{{asset($info_individual->ruta_imagen)}}" width="100%" height="100%"  alt="Alta">
								</a>
							</div>
						</div>
					@endif
				@endforeach

				<!-- Selected Image -->
				

				<!-- Description -->
				<div class="col-lg-5 order-3">
					<div class="product_description">
						<div class="product_name">{{$info_individual->producto_nombre}}</div>
						<div class="rating_r rating_r_4 product_rating">{{$info_individual->codigoInterno}}</div>

						<div class="product_price">${{number_format($info_individual->precio,2)}} MXN</div>
						
						<div class="product_text">
							<p>{{$info_individual->descripcion}} Lorem ipsum dolor sit amet, consectetur adipisicing elit. A nobis, optio officiis! Enim alias cupiditate quisquam ex, asperiores facilis corporis nostrum magnam. Placeat delectus quasi ipsum id illo. Totam, exercitationem.</p>
						</div>
							<div class="order_info d-flex flex-row">							
								<form action="{{url('add_carrito')}}" method="post" enctype="multipart/form-data" id="form_prod" class="col-12">
								 	{{ csrf_field() }} 
								<div class="clearfix">								
								
						  			<div class="product_color" >Selecciona tu talla </div>
						  				<div id="product_color" align="">
											@if($info_individual->txCh)
											<label class="btns" for="ech"><input type="radio" class="talls" name="talla" id="ech" value="ECH" style="display:none ;">ECH</label>
											@endif

											@if($info_individual->tCh)
											<label class="btns" for="ch"><input type="radio" class="talls" name="talla" id="ch" value="CH" style="display: none ;">CH</label>
											@endif

											@if($info_individual->tM)
											<label class="btns" for="m"><input type="radio" class="talls" name="talla" id="m" value="M" style="display:  none;">M</label>
											@endif

											@if($info_individual->tG)
											<label class="btns"  for="g"><input type="radio" class="talls" name="talla" id="g" value="G" style="display: none ;">G</label>
											@endif

											@if($info_individual->txG)
											<label class="btns" for="eg"><input type="radio" class="talls" name="talla" id="eg" value="EG" style="display: none ;">EG</label>
											@endif
										</div>
										<input type="hidden" name="producto" value="{{$info_individual->identifica_prod}}" style="display: none;">
										<input type="hidden" name="idusuario" value="{{Session::get('id_usuario')}}" style="display: none;">
										<input type="hidden" name="precio" value="{{$info_individual->precio}}" style="display: none;">
						  				<input type="hidden" name="nombre_prod" value="{{$info_individual->producto_nombre}}" style="display: none;">						  									  		
						  				<div class="product_color">
						  					<p align="">
						  						<button type="button" class="btn" id="modaltallas" data-toggle="modal" data-target="#myModal">
						  						Guia de tallas
						  						</button>
						  					</p>
						  				</div>						
								    </div>
									
									
										<div class="button_container">
											<input type="submit" id="registro" class="form_prod" value="Añadir al carrito" >											
										</div>
										<div class="button_container">
											<!--<input type="submit" id="registro" value="Añadir a carrito" >-->													
											<button id="favorite"><i class="fa fa-heart"></i>Guardar en Favoritos</button>										
										</div>

										<div class="product_fav">
											<ul>
												<li><i class="fa fa-plus"></i>Info</li>
												<li><i class="fa fa-plus"></i>Cuidados y condiciones</li>
											</ul>
										</div>
									</div>						
								</div><!--fin clearfix-->
								</form>
							</div><!--fin order-info-->
					</div>
				</div><!--fin col-5- order-->
			</div><!--fin row-->

			<div class="container" id="feat">
    <h3 class="h3">Artículos relacionados</h3>
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="product-grid6">
                <div class="product-image6">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo10/images/img-1.jpg">
                    </a>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="#">Men's Shirt</a></h3>
                    <div class="price">$11.00
                        <span>$14.00</span>
                    </div>
                </div>
                <ul class="social">
                    <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                    <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                    <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="product-grid6">
                <div class="product-image6">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo10/images/img-2.jpg">
                    </a>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="#">Women's Red Top</a></h3>
                    <div class="price">$8.00
                        <span>$12.00</span>
                    </div>
                </div>
                <ul class="social">
                    <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                    <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                    <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="product-grid6">
                <div class="product-image6">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo10/images/img-3.jpg">
                    </a>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="#">Men's Shirt</a></h3>
                    <div class="price">$11.00
                        <span>$14.00</span>
                    </div>
                </div>
                <ul class="social">
                    <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                    <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                    <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="product-grid6">
                <div class="product-image6">
                    <a href="#">
                        <img class="pic-1" src="http://bestjquery.com/tutorial/product-grid/demo10/images/img-4.jpg">
                    </a>
                </div>
                <div class="product-content">
                    <h3 class="title"><a href="#">Men's Shirt</a></h3>
                    <div class="price">$11.00
                        <span>$14.00</span>
                    </div>
                </div>
                <ul class="social">
                    <li><a href="" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
                    <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-shopping-bag"></i></a></li>
                    <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
		</div><!---fiin-->
	</div>



@stop