@if(isset($msje))
			{{$msje}}
		@endif

@if(isset($data))
	
	<table class='productos' class="table" >
		<thead>
			<th><div align="center">No de Registro</div></th>
			<th><div align="center">Nombre</div></th>
			<th><div align="center">Perfil</div></th>
		</thead>
	
	@foreach($data as $campo)
		<tr>
			<td><div align="center"><a href="{{url('perfilCliente',$campo->idCliente)}}">{{$campo->idCliente}}</a></div></td>
			<td><div align="center">{{$campo->nombre." ".$campo->appaterno." ".$campo->apmaterno}}</div> </td>
			<td><div align="center">{{$campo->perfil}}</div></td>
		</tr>
	@endforeach
	</table>
@endif