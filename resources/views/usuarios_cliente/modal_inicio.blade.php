<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Inicia Sesion</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @include('errorUser')

                           @if(isset($mensaje))
                             {!!$mensaje!!}

                             @unset($mensaje)
                        @endif
              
              {!!Form::open(['url' => 'login', 'method' => 'POST']) !!}

                <br><br>
                <label for="correo">E-mail</label>
                <input type="text" name="correo" required id="correo" class="form-control" placeholder="E-mail"><br>
                
                <label for="password">Contraseña:</label>
                <input type="password" name="password" required id="password" class="form-control" placeholder="Contraseña">

                <br><br>
                
                <input type="hidden" value="modalInicio" name="modalInicio">

                <div class="sum" style="text-align: center;">
                  <div class="row">
                    <div class="col align-self-center">
                      <input type="submit" id="send" class="form-control btn btn-outline-danger" value="Enviar" style="width:150px;">
                      <p>¿Olvidaste tu contraseña?<br><a href="{{url('registro')}} ">Crea una cuenta</a></p>
                    </div>
                  </div>
                  
                </div>
              {!! Form::close()!!}
      </div>
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div>
  </div>
</div>