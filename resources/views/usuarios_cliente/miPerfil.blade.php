@extends('base2')

@section('content')

@if(isset($id_usuario))

@endif

		<div id="total">
			<div class="contenedor">
				<h1 class="accionar" data-related="guiacompra"><div id="tittleSesion">Mi Perfil</div><div id="tittleRegistre">&nbsp;</div></h1>
			</div>
		</div>
		<br>
		
		
		<div class="container">
			<section id="tabs">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 ">
						@if(isset($msje))
							{{dd($msje)}}
						@endif
						
						@if(\Session::has('msje'))
							{!!\Session::get('msje')!!}
						@endif
							<nav>
								<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
									<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Información</a>
									<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Mis Compras</a>
									<!--<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Favoritos</a>
									<a class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false">...</a>-->
								</div>
							</nav>
							<div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
								<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
									<form action="{{url('updatePerfil')}}" enctype="multipart/form-data" class="col" method="POST">
										{!! csrf_field() !!}  
							        	<div class="row">
								         	<div class="col-lg-6">
								         		<h4>Personal</h4>
										         <label for="namePer" class="mat-label" style="margin-bottom:0px;">Nombre:</label>
										         <input type="text" name="name" id="namePer" title="{{$aviso}}" placeholder="Nombre" value="{{$id_usuario[0]->name}}" class="mat-input" autofocus="autofocus">

										         <label for="appaterno" class="mat-label" style="margin-bottom:0px;">Primer Apellido</label>
										         <input type="text" name="appaterno" id="appaterno" class="mat-input" value="{{$id_usuario[0]->appaterno}}" placeholder="Primer Apellido">

										         <label for="apmaterno" class="mat-label" style="margin-bottom:0px;">Segundo Apellido</label>
										         <input type="text" name="apmaterno" id="apmaterno" class="mat-input" value="{{$id_usuario[0]->apmaterno}}" placeholder="Segundo Apellido">												
												<div class="row">
												<div class="col">
												@if ($errors->has('email'))
					                                <div class="alert alert-danger success-alert" id='success-alert' role="alert">
					                                    <buton type="button" class='close' data-dismiss="alert"> &times;</buton>
					                                    <p>{{$errors->first('email') }}</p>
					                                </div>
					                            @endif 
					                            <label for="email" class="mat-label" style="margin-bottom:0px;">Correo:</label>					                       
					                            <a id="modalMail" data-toggle="modal" data-target="#myModal" class="btn btn-info pull-right"  style="width: 100px; height: 30px; font-size: 10px; z-index: 1; margin-bottom: -15px;">Cambiar correo</a>
									         	<input type="email" name="email" disabled id="email" value="{{$id_usuario[0]->email}}" placeholder="E-mail" class="mat-input">

									         	</div>
									         	<div class="col">
									         		<label for="celular" class="mat-label" style="margin-bottom:0px;">Celular:</label>
									         		<input type="text" name="celular" id="celular" value="{{$id_usuario[0]->celular}}" placeholder="Celular" class="mat-input">
									         	</div>
									         	</div>
									        </div>
								         	
								         	<div class="col-lg-6">
								         		<h4>Domicilio</h4>
								         			<div class="row">
														<div class="col">
															<label for="calle_c" class="mat-label" style="margin-bottom:0px;">Calle:</label>
															<input type="text" name="calle_c" id="calle_c" value="{{$id_usuario[0]->calle}}" class="mat-input" required>
														</div>
												
														<div class="col">															
															<label for="nointerior_c" class="mat-label" style="margin-bottom:0px;">Numero Int:</label>
															<input type="text" name="nointerior_c" id="nointerior_c" value="{{$id_usuario[0]->numero_int}}" class="mat-input">
														</div>
														<div class="col">
															<label for="noexterior_c" class="mat-label" style="margin-bottom:0px;">Numero Ext:</label>
															<input type="text" name="noexterior_c" id="noexterior_c" value="{{$id_usuario[0]->numero_ext}}" class="mat-input" required>
														</div>
													</div>

													<div class="row">
														<div class="col col-md-6">
															<label for="cruzamiento_c" class="mat-label" style="margin-bottom:0px;">Cruzamientos:</label>
															<input type="text" name="cruzamiento_c" id="cruzamiento_c" value="{{$id_usuario[0]->cruzamientos}}" class="mat-input" required>
														</div>
														<div class="col col-md-6">
															<label for="colonia_c" class="mat-label" style="margin-bottom:0px;">Colonia:</label>
															<input type="text" name="colonia_c" id="colonia_c" value="{{$id_usuario[0]->colonia_fracc}}" class="mat-input" required>
														</div>
													</div>

													<div class="row">
														<div class="col col-md-4">
															<label for="ciudad_c" class="mat-label" style="margin-bottom:0px;">Ciudad:</label>
															<input type="text" name="ciudad_c" id="ciudad_c" value="{{$id_usuario[0]->ciudad}}" class="mat-input" required>
														</div>
														<div class="col col-md-4">
															<label for="estado_c" class="mat-label" style="margin-bottom:0px;">Estado:</label>
															<input type="text" name="estado_c" id="estado_c" value="{{$id_usuario[0]->estado}}" class="mat-input" required>
														</div>
														<div class="col col-md-4">
															<label for="cp_c" class="mat-label" style="margin-bottom:0px;">CP:</label>
															<input type="text" name="cp_c" id="cp_c" value="{{$id_usuario[0]->cp}}" class="mat-input" required>
														</div>
													</div>
								         	</div>
							         	</div>
										<br><br>
							         	<div class="row">
								        	<div class="col">
									    		<input type="submit" id="registro" style="margin-top: 5px; width: 50%;" class="btn btn-outline-danger" value="Actualizar">
									    	</div>
									    </div>
    								</form>
								</div>
								<div class="tab-pane fade tableCom" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            						
            						<!--<h4><small>Simple With Actions</small></h4>-->
                						<div class="table-responsive">
								            <table class="table">
								                <thead>
								                    <tr>
								                        <!--<th class="text-center">#</th>-->
							                            <th><div align="center">Estatus</div></th>
							                            <th><div align="right">Monto</div></th>
							                            <th><div align="center">Fecha</div></th>
							                            <!--<th class="text-right">Salary</th>
							                            <th class="text-right">Actions</th>-->
								                    </tr>
								                </thead>
								                <tbody>
													@if (!$compras->isEmpty())
													
								                	@foreach($compras as $compra)
															<tr>
								                            <!--<td class="text-center"></td>-->
								                            	<td><div align="center">{{($compra->estatusPedido == "ENPROCESO") ? "PROCESANDO" : (($compra->estatusPedido == "CANCELADO") ? "CANCELADO2" : "ENVIADO")}}</div></td>
								                            	<td><div align="right">$ {{number_format($compra->costoPedido,2)}}</div></td>
								                            	<td><div align="center">{{date('d-m-Y', strtotime($compra->fecha_levantada))}}</div></td>
								                            </tr>
								                    
								                	@endforeach
								                	@endif
								                        
								                </tbody>
								            </table>
								            <nav aria-label="...">
									    		<ul class="pagination justify-content-center">
									         		{{$compras->links('vendor.pagination.bootstrap-4')}}
									     		</ul>
											</nav>
               							</div>
								</div>
								<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
									Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
								</div>
								<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
									Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat. Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.
								</div>
							</div>						
						</div>
					</div>
				</div>
			</section>
<!-- ./Tabs -->
		</div>

		@if(isset($aviso))
			@php $aviso = "" @endphp
		@endif

@stop