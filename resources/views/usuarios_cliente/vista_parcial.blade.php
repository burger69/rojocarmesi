		<!--<h4><small>Simple With Actions</small></h4>-->
                						<div class="table-responsive">
								            <table class="table">
								                <thead>
								                    <tr>
								                        <!--<th class="text-center">#</th>-->
							                            <th><div align="center">Estatus</div></th>
							                            <th><div align="right">Monto</div></th>
							                            <th><div align="center">Fecha</div></th>
							                            <!--<th class="text-right">Salary</th>
							                            <th class="text-right">Actions</th>-->
								                    </tr>
								                </thead>
								                <tbody>
													@if (!$compras->isEmpty())
													
								                	@foreach($compras as $compra)
															<tr>
								                            <!--<td class="text-center"></td>-->
								                            	<td><div align="center">{{($compra->estatusPedido == "ENPROCESO") ? "PROCESANDO" : (($compra->estatusPedido == "CANCELADO") ? "CANCELADO2" : "ENVIADO")}}</div></td>
								                            	<td><div align="right">$ {{number_format($compra->costoPedido,2)}}</div></td>
								                            	<td><div align="center">{{date('d-m-Y', strtotime($compra->fecha_levantada))}}</div></td>
								                            </tr>
								                    
								                	@endforeach
								                	@endif
								                        
								                </tbody>
								            </table>
								            <nav aria-label="...">
									    		<ul class="pagination justify-content-center">
									         		{{$compras->links('vendor.pagination.bootstrap-4')}}
									     		</ul>
											</nav>
               							</div>