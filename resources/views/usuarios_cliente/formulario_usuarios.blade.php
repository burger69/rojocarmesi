     <br>
     <div class="container">
         
          <ul class="nav nav-tabs">
               <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home">Alta Usuario</a>
               </li>
               <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#menu1">Lista Usuarios</a>
               </li>
          </ul>

          <div class="col-lg-12" id='contents'>
               @if(isset($mensaje))
                    {!!$mensaje!!}
               @endif
               
               <div class="tab-content"><br>
                    <div class="tab-pane container active" id="home">          
                      <form action="{{url('alta_usuario')}}"  enctype="multipart/form-data" id="form_cliente" class="col-12" method="POST">
                         <h3>Alta de Usuario</h3>
                         <label for="name">Nombre:</label>
                         <input type="text" name="name" id="name"  class="form-control">

                         <label for="appaterno">Primer Paterno</label>
                         <input type="text" name="appaterno" id="appaterno" class="form-control" >

                         <label for="apmaterno">Segundo Materno</label>
                         <input type="text" name="apmaterno" id="apmaterno" class="form-control" >

                         <label for="mailR">Correo:</label>
                         <input type="email" name="mailR" id="mailR"  class="form-control">

                         <label for="mail2">Confirmar Correo:</label>
                         <input type="email" name="mail2" id="mail2" class="form-control">

                         <label for="password">Contraseña:</label>
                         <input type="password" name="password" id="password" class="form-control" >

                         <label for="password2">Confirmar Contraseña:</label>
                         <input type="password" name="password2" id="password2" class="form-control">
                         <br>
                         <label for="perfil_alta">Rol del usuario:</label>
                         <select name="perfil_alta" id="perfil_alta" class="form-control">
                              <option value="">Seleccione una opción</option>
                              <option value="ADMIN">Administrador</option>
                              <!--<option value="CLIENTE">Cliente</option>-->
                         </select>

                         <br>
                         <input type="submit" id="send" class="btn btn-outline-danger" value="Guardar">
                         </form> <br><br><br><br>
                    </div>
                    
                    <div class="tab-pane container fade" id="menu1">
                         <div class="table-responsive">
                              @if(isset($conten))

                                   {!!$conten!!}
          
                              @endif
                         </div>
                         <br>
                         <br>
                         <br>
                    </div>
               </div>
          </div>
     </div>    