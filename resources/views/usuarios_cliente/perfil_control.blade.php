{!!Form::open(['url' => 'updatePerfil', 'enctype'=>'multipart/form-data', 'class'=>'col-16', 'method' => 'POST']) !!}
         <h3></h3>

         <div class="row">
         	<div class="col-md-6">
         		<h4>Información Personal</h4>
		         <label for="name">Nombre:</label>
		         <input type="text" name="name" id="name" placeholder="Nombre" value="{{$id_usuario->nombre}}" class="form-control">

		         <label for="appaterno">Primer Apellido</label>
		         <input type="text" name="appaterno" id="appaterno" class="form-control" value="{{$id_usuario->appaterno}}" placeholder="Primer Apellido">

		         <label for="apmaterno">Segundo Apellido</label>
		         <input type="text" name="apmaterno" id="apmaterno" class="form-control" value="{{$id_usuario->apmaterno}}" placeholder="Segundo Apellido">

		         <label for="mailR">Correo:</label>
		         <input type="email" name="mailR" id="mailR" value="{{$id_usuario->correo}}" disabled placeholder="E-mail" class="form-control">
	         </div>
	         <div class="col-md-6">
	         	<h4>Dirección</h4>
	         	<div class="row">
					<div class="col">
						<label for="calle_c">Calle:</label>
						<input type="text" name="calle_c" id="calle_c" value="{{$id_usuario->calle}}" class="form-control" required>
					</div>
					
					<div class="col">
						<label for="nointerior_c">Numero Int:</label>
						<input type="text" name="nointerior_c" id="nointerior_c" value="{{$id_usuario->numero_int}}" class="form-control">
					</div>
					<div class="col">
						<label for="noexterior_c">Numero Ext:</label>
						<input type="text" name="noexterior_c" id="noexterior_c" value="{{$id_usuario->numero_ext}}" class="form-control" required>
					</div>
				</div>

				<div class="row">
					<div class="col col-md-6">
						<label for="cruzamiento_c">Cruzamientos:</label>
						<input type="text" name="cruzamiento_c" id="cruzamiento_c" value="{{$id_usuario->cruzamientos}}" class="form-control" required>
					</div>
					<div class="col col-md-6">
						<label for="colonia_c">Colonia:</label>
						<input type="text" name="colonia_c" id="colonia_c" value="{{$id_usuario->colonia_fracc}}" class="form-control" required>
					</div>
				</div>
				<div class="row">
					<div class="col col-md-4">
						<label for="ciudad_c">Ciudad:</label>
						<input type="text" name="ciudad_c" id="ciudad_c" value="{{$id_usuario->ciudad}}" class="form-control" required>
					</div>
					<div class="col col-md-4">
						<label for="estado_c">Estado:</label>
						<input type="text" name="estado_c" id="estado_c" value="{{$id_usuario->estado}}" class="form-control" required>
					</div>
					<div class="col col-md-4">
						<label for="cp_c">CP:</label>
						<input type="text" name="cp_c" id="cp_c" value="{{$id_usuario->cp}}" class="form-control" required>
					</div>
				</div>
	         </div>
         </div>
		<br><br>
        <div class="row">
	       	<div class="col">
		    		<input type="submit" id="send" class="btn btn-outline-danger" value="Actualizar">    		
		   			<a href="{{url('eliminar_user',$id_usuario->identi_cliente)}}" class="btn btn-danger">Eliminar</a>
		   	</div>
		   	<div class="col">
		   		
		   	</div>
		</div>
    {!! Form::close()!!}