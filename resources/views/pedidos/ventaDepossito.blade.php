@extends('base')

@section('content')
<div class="container" style="height: 400px; border:10">
		<div class="col col-lg-12"><br><br>
			<div class="card bg-faded" style="text-align:center; height: 250px;">
				<div class="card-block"><br><br>
					<img data-imagetype="External" src="{{asset('img/admira.jpg')}}" alt="Congrats" width="64" height="64"> <span style="font-family:Arial; font-size:20px; color:#DC0330; font-weight:normal">¡Ya falta poco para tu compra!</span>
					<br>
					<p>Te enviamos a tu correo la referencia para el pago para concluir con la compra.</p>
					<br>
					<a href="{{url('/')}} ">Seguir comprando...</a>	
     			</div>
			</div>
		</div>
	</div>
</div>
@stop