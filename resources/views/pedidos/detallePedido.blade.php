<div>
	<br>
		<div id="datos_envio">
				<h5>Datos de Envío</h5>
				@if(isset($data))
				<div class="row">
					<div class="col-md-10">
						Nombre: <br>
						<label>{{$data[0]->nombre_sin.' '.$data[0]->appaterno.' '.$data[0]->apmaterno}}</label>
						<!--<input type="text" name="nombre_c" value="{{$data[0]->nombre}}" id="nombre_c" class="form-control">-->
					</div>
				</div>

				<div class="row">
					<div class="col-md-10">
						Direccion: <br>
						<label for="calle_c">C. {{$data[0]->calle.' '.$data[0]->numero_int}} No. Int {{$data[0]->numero_int}} No. Ext {{$data[0]->numero_ext.' por '.$data[0]->cruzamientos}} Col. {{$data[0]->colonia_fracc.' '.$data[0]->ciudad.' '.$data[0]->estado .' '.$data[0]->cp}}  </label>
					</div>
				</div><br>
				@else
					<div class="row">
					<div class="col-md-10">
						<label for="nombre_c">Nombre:</label>
						
						<input type="text" name="nombre_c" value="{{  isset($data[0]->nombre_sin) ? $data[0]->nombre_sin : '' }}" id="nombre_c" class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-md-2">
						Direccion: <br>
						<label for="calle_c">Calle:</label>
						<input type="text" name="calle_c" id="calle_c" value="" class="form-control" required>
					</div>
					<div class="col-md-2">
						<br>
						<label for="nointerior_c">Numero Int:</label>
						<input type="text" name="nointerior_c" id="nointerior_c" value="" class="form-control">
					</div>
					<div class="col-md-2">
						<br>
						<label for="noexterior_c">Numero Ext:</label>
						<input type="text" name="noexterior_c" id="noexterior_c" value="" class="form-control" required>
					</div>
					<div class="col-md-3">
						<br>
						<label for="cruzamiento_c">Cruzamientos:</label>
						<input type="text" name="cruzamiento_c" id="cruzamiento_c" value="" class="form-control" required>
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
						<label for="colonia_c">Colonia:</label>
						<input type="text" name="colonia_c" id="colonia_c" value="" class="form-control" required>
					</div>
					<div class="col-md-3">
						<label for="ciudad_c">Ciudad:</label>
						<input type="text" name="ciudad_c" id="ciudad_c" value="" class="form-control" required>
					</div>
					<div class="col-md-3">
						<label for="estado_c">Estado:</label>
						<input type="text" name="estado_c" id="estado_c" value="" class="form-control" required>
					</div>
					<div class="col-md-2">
						<label for="cp_c">CP:</label>
						<input type="text" name="cp_c" id="cp_c" value="" class="form-control" required>
					</div>
				</div><br>
				@endif
				<div class="row">
						<div class="col-md-3">						
							<form action="{{url('cambio_status')}}" method="post" >
								{{ csrf_field() }} 
							<label for="estatusPedido">Estatus del Pedido</label>
							<select name="estatusPedido" id="estatusPedido" class="form-control">
								<option value="{{$data[0]->estatusPedido}}">{{($data[0]->estatusPedido == 'ENPROCESO') ? 'EN PROCESO' : $data[0]->estatusPedido}}</option>
								<option value="ENVIADO">ENVIADO</option>
								<option value="CANCELADO">CANCELADO</option>
							</select>
						</div>
							<div class="col-md-2" style="margin-top:35px;">
								<input type="hidden" value="{{$data[0]->idpedido}}" name="identifica_pedido">
								<!--<a href="{{url('cambio_status',$data[0]->idpedido)}}" class="btn btn-success">Cambiar Estatus</a>-->
								<!--<input type="submit" value="Cambiar Estatus" >-->
								<button class="btn btn-success">Cambiar Estatus</button>
							</div>
						</form>
				</div>	<br>			
			</div>
		</div>

		<div class="row">
        <div class="col col-md-10"><br>                <!-- Cart -->
                <table class="table cart-table responsive-table">
                <thead style="background: #DC0330; border:0px;">
                    <th>
                         <div align="center">Imagen</div>
                    </th>
                    <th>
                         <div align="center">Descripcion</div>
                    </th>
                    <th>
                         <div align="center">Talla</div>
                    </th>
                    <th>
                         <div align="center">Cantidad</div>
                    </th>
                    <th>
                         <div align="center">Precio</div>
                    </th>
                    <!--<th><div align="center">Quitar</div>-->
                    </th>
                </thead>
                <!-- Item #1 -->
                @if(isset($dataPedido) && count($dataPedido) > 0 )
                    @php $indexTalla = 0; @endphp
                    @foreach($dataPedido as $valor)
               			 @foreach($valor as $valor2)
                    	
	                    	<tr>
	                    		<td><div align="center"><img width="70" height="80" src="{{ asset($valor2['ruta_imagen'])  }}" alt=""></div></td>
	                    		<td><div align="center">{{ $valor2['producto_nombre']  }}</div></td>
	                    		<td><div align="center">{{ $tallasPedido['tallas'][$indexTalla]  }}</div></td>
	                    		<td><div align="center">{{ $tallasPedido['cantidad'][$indexTalla] }}</div></td>
	                    		<td><div align="center">${{ number_format($valor2['precio'],2)  }}</div></td>
	                    		<!--<td><div align="center"><a href="#" class="btn btn-danger">X</a></div></td>-->
	                    		

	                    	</tr>
	                   	@php $indexTalla++; @endphp
                    	@endforeach
                    @endforeach
                  @endif                  
                </table>
        </div>

</div>