<div class="container">
  <h2>Pedidos</h2>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#home">Activos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu1">Cancelado</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#menu2">Enviados</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
      <div id="home" class="container tab-pane active"><br>
        <table class="table productos">
          <thead>
            <th><div align="center">No. Pedido</div></th>
            <th><div align="center">Monto</div></th>
            <th><div align="center">Fecha</div></th>
          </thead>
          @if(isset($proceso['Activos']))
          @php $Activos = $proceso['Activos']; @endphp
              @foreach($Activos as $Pedido)
                <tr>
                  <td><div align="center"><a href="{{url('detallePedido',$Pedido->idpedido)}}" class="pedido">{{$Pedido->idpedido}}</a></div></td>
                  <td><div align="center">${{number_format($Pedido->costoPedido,2)}}</div></td>
                  <td><div align="center">{{$Pedido->fecha_levantada}}</div></td>
                </tr>
            @endforeach
          @endif        
        </table>
      </div>

      <div id="menu1" class="container tab-pane fade"><br>
        <table class="table productos">
          <thead>
            <th><div align="center">No. Pedido</div></th>
            <th><div align="center">Monto</div></th>
            <th><div align="center">Fecha</div></th>
          </thead>
          @if(isset($proceso['Cancelados']))
            @php $cancelado = $proceso['Cancelados']; @endphp
            @foreach($cancelado as $valor2)
                <tr>
                  <td><div align="center"><a href="{{url('detallePedido',$valor2->idpedido)}}" class="pedido">{{$valor2->idpedido}}</a></div></td>
                  <td><div align="center">${{number_format($valor2->costoPedido,2)}}</div></td>
                  <td><div align="center">{{$valor2->fecha_levantada}}</div></td>
                </tr>
            @endforeach
          @endif        
        </table>
      </div>

      <div id="menu2" class="container tab-pane fade"><br>
        <table class="table productos">
          <thead>
            <th><div align="center">No. Pedido</div></th>
            <th><div align="center">Monto</div></th>
            <th><div align="center">Fecha</div></th>
          </thead>
          @if(isset($proceso['Enviados']))
            @php $Enviados = $proceso['Enviados']; @endphp
            @foreach($Enviados as $valor3)
                <tr>
                  <td><div align="center"><a href="{{url('detallePedido',$valor3->idpedido)}}"  class="pedido">{{$valor3->idpedido}}</a></div></td>
                  <td><div align="center">${{number_format($valor3->costoPedido,2)}}</div></td>
                  <td><div align="center">{{$valor3->fecha_levantada}}</div></td>
                </tr>
            @endforeach
          @endif        
        </table>
      </div>
  </div>
</div>