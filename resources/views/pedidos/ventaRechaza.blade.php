@extends('base')

@section('content')
<div class="container" style="height: 400px; border:10">
		<div class="col col-lg-12"><br><br>
			<div class="card bg-faded" style="text-align:center; height: 250px;">
				<div class="card-block"><br><br>
					<img data-imagetype="External" src="{{asset('img/tache.png')}}" alt="Congrats" width="64" height="64"> <span style="font-family:Arial; font-size:20px; color:#DC0330; font-weight:normal">¡Transacción Rechazada!</span>
					<br>
					<p>No se acompleto la transaccion de tu pago.</p>
					<br>
					<a href="{{url('vista_carrito')}} ">Ver mi carrito...</a>	
     			</div>
			</div>
		</div>
	</div>
</div>
@stop