@extends('base')


@section('content')
<div class="container" style="height: 400px; border:10">
		<div class="col col-lg-12"><br><br>
			<div class="card bg-faded" style="text-align:center; height: 250px;">
				<div class="card-block"><br><br>
					<img data-imagetype="External" src="{{asset('img/palomita.gif')}}" alt="Congrats" width="32" height="32"> <span style="font-family:Arial; font-size:20px; color:#DC0330; font-weight:normal">¡Tu compra se ha completado con éxito!</span>
					<br>
					<p>Se envió un correo con el detalle de la compra</p>
					<br>
					<a href="{{url('/')}} ">Seguir comprando...</a>	
     			</div>
			</div>
		</div>
	</div>
</div>
@stop