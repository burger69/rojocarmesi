@extends('base2')

@section('content')
	@if(isset($avisoSinDomicilio))
		{!!$avisoSinDomicilio!!}
	@endif

    <section class="text-center">
        <div class="container">
            <h1>MIS PRODUCTOS</h1>
         </div>
    </section>

    <div class="container mb-4">
    <!--<div class="row">-->
        <div class="col-12">
            <div id="datos_envio"></div>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"><center>Imagen</center></th>
                            <th scope="col"><center>Producto</center></th>                            
                            <th scope="col" class="text-center"><center>Talla</center></th>
                            <th scope="col" class="text-center"><center>Cantidad</center></th>
                            <th scope="col"><center>Precio unitario</center></th>
                            <th scope="col" class="text-right"><center>Precio</center></th>
                            <th> </th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $Total = 0; $activo = ""; @endphp

                        @if($articulos_compra != NULL)	

                        @php $activo = url('continuar'); @endphp					
	                        @foreach($articulos_compra as $valor)


                                @php $precioUni = $valor['precio']; @endphp

	                        	<tr>
		                            <td><div align="center"><img id="imgMail" width="70" height="80" src="{{ asset($valor['ruta_img'])  }}" alt=""></div> </td>
		                            <td><center>{{ $valor['nombre_producto']}}</center></td>	
                                    <td><center>{{ $valor['talla']}}</center></td>    	                            
		                            <td>
                                        <center>
                                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                            <input type="number" name="{{$valor['identi_trasaccion']}}" id="{{$valor['identi_trasaccion']}}" step="1" max="31" min="1" value="{{ $valor['cantidad_prod']  }}"  class="form-control" style="width: 100px;">
                                        </center>
                                    </td>
                                    <td><center>${{ number_format($valor['precioUni'],2) }}</center></td>
		                            <td class="text-right"><center>${{ number_format($valor['precio'],2) }}</center></td>
		                            <td class="text-right"><a class="btn btn-danger" href="{{url('quitar',$valor['identi_trasaccion'])}}"><i class="fa fa-trash"></i></a></td>
	                        	</tr>
	                        	@php $Total = $Total+$valor['precio']; @endphp
	                        @endforeach

	                    @else

	                    	<tr>
	                    		<td colspan="7"><center><div class="alert alert-danger">Sin productos</div> </center></td>
	                    	</tr>
                        @endif
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Sub-Total</td>
                            <td class="text-right">${{ number_format(round($Total/1.16,2),2) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>IVA</td>
                            <td class="text-right">${{ number_format(round($Total/1.16,2)*.16,2) }}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><strong>Total</strong></td>
                            <td class="text-right"><strong>${{number_format($Total,2)}}</strong></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col mb-2">
            <div class="row">
                <div class="col-sm-12  col-md-6">
                    <button class="btn btn-block btn-light"><a href="{{url('/')}}">Continuar Comprando</a></button>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <button {{$activo}} class="btn btn-lg btn-block btn-success text-uppercase"  onclick="window.location='{{$activo}}';">Continuar con el pago</button>
                </div>
            </div>
        </div>
        <!--</div>-->
    </div>
	
	@if(isset($avisoUsuario) and $avisoUsuario == true)
		@include('usuarios_cliente.modal_inicio')
    @endif
@stop