@extends('base2')

@section('content')
	<br>

	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-8">
				@if(isset($avisoSinDomicilio))
					{!!$avisoSinDomicilio!!}
				@endif
	            <form action="{{url('terminarPedido')}}" id="form_comprar" method="post" enctype="multipart/form-data" class="form-horizontal">  
	                {!! csrf_field() !!}                          
	                    <div class="card">
	                        <div class="card-header">
	                            <strong>Datos del Comprador</strong>
	                        </div>

	                        <div id="datos_envio"></div>
	                
	                    	<div class="card-body card-block">
	                       		<div id="ajax"></div>
	                        	<h5>Información Personal</h5> 

	                        	<div class="row form-group">
		                            <div class="col-12 col-lg-6">
		                            	<div class="mat-div">		                         
		                                	<input type="text" placeholder="Nombre" name="nombre_c" value="{{ isset($datosDomicilio['nombre_usuario']) ? $datosDomicilio['nombre_usuario'] : ''}}" id="nombre_c"  class="mat-input">	
		                                </div>	                           
		                            </div>
									<br><br>
									<div class="col-12 col-lg-6">
										<div class="mat-div">	
		                                	<input type="text" placeholder="Apellidos" name="apellido_c" value="{{ isset($datosDomicilio['appaterno_usuario']) ? $datosDomicilio['appaterno_usuario'] : ''}}" id="apellido_c" class="mat-input">
		                                </div>
		                            </div>
	                        	</div>

	                        	<div class="row form-group">
		                            <div class="col-6 col-md-4">
		                            	<div class="mat-div">
		                                	<input type="text" placeholder="Celular" name="celular_c" value="" id="celular_c"  class="mat-input">
		                                </div>
		                            </div>
									
									<div class="col-6 col-md-4">
										<div class="mat-div">
		                                	<input type="email" placeholder="Correo" name="correo_c" value="{{Session::get('correo')}}" id="correo_c" class="mat-input">
		                                </div>
		                            </div>
	                        	</div>
	                        	<h5>Direccion de Envío</h5>

	                        	<div class="row form-group">
		                            <div class="col-6 col-md-3">
		                            <div class="mat-div">		                         
		                            	    <input type="text" name="calle_c" placeholder="Calle" id="calle_c" value="{{$datosDomicilio['calle']}}" class="mat-input" required>		                             
		                            	   </div>
		                            </div>
									
									<div class="col-6 col-md-3">
										<div class="mat-div">
		                                	<input type="text" name="nointerior_c" placeholder="Numero Int." id="nointerior_c" value="{{$datosDomicilio['numero_int']}}" class="mat-input">
		                                </div>
		                            </div>
									<br><br>
		                            <div class="col-6 col-md-3">
		                            	<div class="mat-div">
		                                	<input type="text" name="noexterior_c" placeholder="Número Ext." id="noexterior_c" value="{{$datosDomicilio['numero_ext']}}" class="mat-input" required>
		                                </div>
		                            </div>

		                            <div class="col-6 col-md-3">
		                            	<div class="mat-div">
		                                	<input type="text" name="cruzamiento_c" id="cruzamiento_c" placeholder="Cruzamientos" value="{{$datosDomicilio['cruzamientos']}}" class="mat-input" required>
		                                </div>
		                            </div>
	                        	</div>

	                        	<div class="row form-group">
		                            <div class="col-12 col-md-4">
		                            	<div class="mat-div">
		                                	<input type="text" name="colonia_c" id="colonia_c" placeholder="Colonia" value="{{$datosDomicilio['colonia_fracc']}}" class="mat-input" required>
		                                </div>
		                            </div>
										<br><br>
									<div class="col-12 col-md-3">
										<div class="mat-div">
		                                	<input type="text" name="ciudad_c" id="ciudad_c" placeholder="Ciudad" value="{{$datosDomicilio['ciudad']}}" class="mat-input" required>
		                                </div>
		                            </div>
										<br><br>
		                            <div class="col-6 col-md-3">
		                            	<div class="mat-div">
		                                	<input type="text" name="estado_c" id="estado_c" placeholder="Estado" value="{{$datosDomicilio['estado']}}" class="mat-input" required>
		                                </div>
		                            </div>
			
		                            <div class="col-6 col-md-2">
		                            	<div class="mat-div">
		                                	<input type="text" name="cp_c" id="cp_c" placeholder="C. P." value="{{$datosDomicilio['cp']}}" class="mat-input" required>
		                                </div>
		                            </div>
	                        	</div>
								
								
	                    	</div><!--Hol-->
	                    </div>
	        </div>




	        <div class="col-lg-4">
                    <div class="widget">
                        <h4 class="widget-title">Resumen de la orden</h4>
                        <div class="summary-block">
                            <div class="summary-content">
                                <div class="summary-head"><h5 class="summary-title">Producto</h5></div>
                                <div class="summary-price">
                                    <p class="summary-text">${{number_format($datosDomicilio['subtotal'],2)}}</p>
                                </div>
                            </div>
                        </div>

                        <div class="summary-block">
                            <div class="summary-content">
                               <div class="summary-head"> <h5 class="summary-title">Total</h5></div>
                                <div class="summary-price">
                                    <p class="summary-text payneto">${{number_format($datosDomicilio['totalNeto'],2) }}</p>
                                </div>
                            </div>
                        </div><br>
                        <div ><input type="submit" value="Mercado Pago" style=" width: 100%; border-radius: 20px !important;" class="btn btn-danger" id="generarPago"></div><br>
                        <div id="paypal-button-container"></div>                      
                    </div>                   
                <!--</div>-->
            </div>
	        </div>

	    </div><!--row-->
	</div>

@stop