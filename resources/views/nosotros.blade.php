@extends('base2')


@section('content')
	
	
		<div id="total">
			<div class="contenedor">
				<h1><div id="tittleSesion">Sobre Rojo Carmesí</div><div id="tittleRegistre">&nbsp;</div></h1>
			</div>
		</div>
		<br>
		<div class="cuerpoacerca">
			<div class="contenedor">
				<div class="row">
					<div class="col-lg-3 icons">
						<ul>
							<li><a class="accionar quiensomos" href="#" data-related="quiensomos">Quienes Somos</a></li>
							<li><a class="" href="#" data-related="privacidad" id="privacidad">Privacidad</a></li>
						</ul>
					</div>
					<div class="col-lg-9 guia">
						<div class="find"  id="quiensomos">

							<h3>•	QUIÉNES SOMOS</h3>

							<p style="text-align: justify;">Somos una marca de ropa 100% mexicana que brinda una propuesta de moda con rasgos de la cultura y folclore mexicano. Cada una de nuestras prendas muestra diversidad de materiales, elementos y texturas que involucran un trabajo artesanal y cientos de años de tradición, ¡Porque lo hecho en México, está bien hecho!
							</p>


							<h2>•	MISIÓN</h2>

							<br>

							<p style="text-align: justify;">En colaboración con productores locales, buscamos enaltecer los rasgos y colores  del folclore mexicano por medio del diseño, fabricación y comercialización de prendas de moda contemporánea, brindando a cada uno de nuestros clientes el  sentimiento de satisfacción total hacia las prendas y del orgullo hacia nuestra cultura. </p>


							<h2>•	VISIÓN</h2>
							
							<br >

							<p style="text-align: justify;">Ser reconocidos como la principal marca de moda mexicana que, al colaborar con productores locales, haga llegar a cada rincón del mundo una pisca de México y su cultura. </p>
						</div>

						<div class="find" style="display:none;" >
							<h3>hola</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
@stop