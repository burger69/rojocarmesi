@extends('base2')

@section('content')
	
	
		<div id="total">
			<div class="contenedor">
				<h1 class="accionar" data-related="guiacompra"><div id="tittleSesion">Guía de compra</div><div id="tittleRegistre">&nbsp;</div></h1>
				
			</div>
		</div>
		<br>
		<div class="cuerpoacerca">
			<div class="contenedor">
				<div class="row">
					<div class="col-lg-3 icons">
						<ul>
							<li><a class="accionar tallas_cuidados" href="#" data-related="tallas_cuidados">Tallas y cuidados</a></li>
							<li><a class="accionar entregas" href="#" data-related="entregas">Entregas</a></li>
							<li><a class="accionar cambios" href="#" data-related="cambios">Cambios</a></li>
							<li><a class="accionar pagos" href="#" data-related="pagos">Pagos</a></li>
						</ul>
					</div>
					<div class ="col-lg-9 guia">
						<div class="find" id="guiacompra">

							<!--<h3>•	COMO COMPRAR</h3>-->
							<p>Nuestro proceso de compra es muy sencillo: <br>
								<ol>
									<li>Elige la sección (mujer, hombre) y la prenda que te interese adquirir.</li>
									<li>Selecciona las características de la/las prendas y confírmalas: tallas disponibles, modelos y precio.</li>
									<li>Añade el artículo a tu carrito de compra y a continuación puedes elegir "seguir comprando" o "finalizar compra".</li>
									<li>Si quieres seguir comprando, repite el proceso.</li>
									<li>Si has terminado de comprar elige el botón "Finalizar compra" y selecciona "Inicia sesión/Regístrate", <br> en caso de que ya seas o quieras ser parte de nuestros clientes frecuentes.</li>
									<li>Una vez que hayas introducido tus datos, comprueba que sean correctos y pulsa &quot;confirmar&quot;.</li>
									<li>Elige tus características de envío: recibirlo por paquetería o a domicilio (únicamente en la
								ciudad de Puebla) <br> y asegúrate de que la dirección e información de envío y facturación son
								correctas.</li>
									<li>Selecciona un método de pago: Visa, Mastercard, Paypal y finalmente confirma el pedido.</li>
									<li>Recibirás un e-mail confirmando tu pedido.</li>
								</ol>
						</div>

						<div class="find" style="display:none;"  id="tallas_cuidados">
							
							<!--<h3>•	CUIDADO DE PRENDAS</h3>-->
							<p>El lino es una tela fresca y es ideal para la época de calor, sobre todo en primavera o verano.<br>
							Al ser una fibra natural, requiere de un cuidado especial para que no se dañe y se mantenga en perfectas condiciones. </p>
								<h4>-	Lavado a mano:</h4>
								<ul>
									<li>o	Jabón neutro.</li>
									<li>o	Agua tibia (no caliente porque puede provocar que se encoja).</li>
									<li>o	Sin exprimir.</li>
									<li>o	Secar a la sombra.</li>
								</ul>
								<h4>-	Lavado a máquina:</h4>
								<ul>
									<li>o	Jabón neutro.</li>
									<li>o	Agua tibia (no caliente porque puede provocar que se encoja).</li>
									<li>o	Colocar la ropa dentro de una bolsa para prendas delicadas.</li>
									<li>o	Sin centrifugar.</li>
								</ul>
								<h4>-	Planchado:</h4>
								<ul>
									<li>o	Planchar la prenda cuando aún se encuentre húmeda y al revés en las áreas con bordados. </li>
								</ul>
								<h4>-	Mantener en lugares secos y ventilados.</h4>
						</div>

						<div class="find" style="display:none;"  id="entregas">

							<!--<h3>•	ENTREGAS Y ENVÍOS</h3>-->
							<p>El tiempo de entrega da inicio a partir de la fecha de confirmación de pago, <br>pudiendo variar de 1 a 5 días para la entrega. La confirmación del pago puede demorar <br> hasta 24 hrs en los siguientes casos: </p>
								<ul>
									<li>o	Depósito ventanilla </li>
									<li>o	Transferencia electrónica</li>
								</ul>
							<p>Rojo Carmesí programará la entrega a la dirección indicada en el campo "dirección de entrega", <br> información que deberá de ser capturada en el formulario de compra de la tienda en línea; <br>las entregas serán realizadas en días hábiles de lunes a viernes a excepción de días festivos.</p>
							<p>El envío no tiene costo extra y las entregas en la Ciudad de Puebla son directamente a domicilio.</p>
							<p>Rojo Carmesí México no opera directamente el envío de la mercancía, el cual es contratado a través de un tercero, <br> con el cual el cliente acepta los términos y condiciones de dicha compañía al momento de realizar la compra. </p>
						</div>

						<div class="find" style="display:none;"  id="cambios">

							<!--<h3>•	CAMBIOS</h3>-->
							<p>Rojo Carmesí admitirá la devolución y/o cambios de mercancía absorbiendo los gastos de envío en los siguientes casos: </p>
								<ul>
									<li>o	Envío de mercancía diferente a la talla, color o modelo solicitado en la tienda en línea. </li>
									<li>o	Envío de mercancía con defecto de fabricación. </li>
									<li>o	Envío realizado a una dirección de entrega diferente a la indicada en el formulario de compra. </li>
								</ul>
							<p>Las solicitudes de devolución y/o cambios se harán efectivas previa validación de la mercancía <br> notificada y con un plazo máximo de 10 días después de la recepción.</p>
							<p>Para dicha solicitud se deberá de enviar un correo a las direcciones de contacto y servicio el cliente, <br> adjuntando fotografía del desperfecto y con una breve explicación de la situación. </p>
						</div>

						<div class="find" style="display:none;"  id="pagos">

							<!--<h3>•	PAGOS</h3>-->
							<p>Rojo Carmesí pone a tu disposición los siguientes métodos de pago para que <br> puedas realizar tus compras a través de nuestra tienda en línea de una manera <br> fácil, rápida y segura.</p>
								<ul>
									<li><b>o	Pago con tarjetas bancarias:</b> Procesado por medio del sistema de cobro <br> MercadoPago, los pagos son realizados de forma segura aceptando todas las <br> tarjetas de crédito o débito Visa y MasterCard.  </li><br>
									<li><b>o	Pago con depósito o transferencia:</b> Pagos por medio de depósitos o transferencias <br> a la cuenta bancaria Rojo Carmesí Banamex. Para mayor <br> información favor de contactar con el personal autorizado a los siguientes <br> números de atención y cuentas de correo. <br> Correo: <a href="servicioalcliente@rojocarmesimx.com">servicioalcliente@rojocarmesimx.com</a></li>
						</div>
					</div>
				</div>
			</div>
		</div>
@stop