<div class="table-responsive">
<table class="table table-striped">
	 	<thead>
	 		<th scope="col">Talla del fabricante</th>
	 		<th scope="col"><div align="center">Medida del Pecho</div></th>
	 		<th scope="col"><div align="center">Talla de Cuello</div></th>
	 		<th scope="col"><div align="center">Largo de Mangas</div></th>
	 	</thead>

	 	<tr scope="row">
	 		<td><div align="left">ECH</div></td>
	 		<td><div align="center">32" - 34"</div></td>
	 		<td><div align="center">13" - 13.5"</div></td>
	 		<td><div align="center">32" - 33"</div></td>	 		
	 	</tr>

	 	<tr scope="row">
	 		<td><div align="left">CH</div></td>
	 		<td><div align="center">35" - 37"</div></td>
	 		<td><div align="center">14" - 14.5"</div></td>
	 		<td><div align="center">33" - 34"</div></td>	 		
	 	</tr>

	 	<tr scope="row">
	 		<td><div align="left">M</div></td>
	 		<td><div align="center">38" - 40"</div></td>
	 		<td><div align="center">15" - 15.5"</div></td>
	 		<td><div align="center">34" - 35"</div></td>	 		
	 	</tr>

	 	<tr scope="row">
	 		<td><div align="left">G</div></td>
	 		<td><div align="center">41" - 43"</div></td>
	 		<td><div align="center">16" - 16.5"</div></td>
	 		<td><div align="center">35" - 36"</div></td>	 		
	 	</tr>

	 	<tr scope="row">
	 		<td><div align="left">ECH</div></td>
	 		<td><div align="center">44" - 46"</div></td>
	 		<td><div align="center">17" - 17.5"</div></td>
	 		<td><div align="center">36" - 37"</div></td>	 		
	 	</tr>
	 </table>
</div>

    




