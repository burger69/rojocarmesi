@extends('base')


@section('content')	
s
	<div class="container">
		<div class="card">
			<div class="row">
				<aside class="col-sm-7 border-right">
					<article class="gallery-wrap">					
						@foreach($info_producto as $info_individual)													
							@if($info_individual->portada_img == 1)	
								<div class="img-big-wrap">
		  							<!--<div>-->
		  								<a href="{{asset($info_individual->ruta_imagen)}}" id="zom" title="{{$info_individual->producto_nombre}}" data-subtitle="Vista Previa" data-fancybox="preview" data-height="1000" data-caption="<strong>{{$info_individual->descripcion}}</strong>">
												<img id="portada" data-zoom-image="{{asset($info_individual->ruta_imagen)}}}}" class="zoom_01" src="{{asset($info_individual->ruta_imagen)}}" width="100%" height="100%"  alt="Alta">
											</a>
		  							<!--</div>-->
								</div> <!-- slider-product.// -->
								<div class="img-small-wrap">
							@else
							<div class="item-gallery"> <img class="imgss" src="{{asset($info_individual->ruta_imagen)}}"> </div>
							@endif
																	
						@endforeach
						</div> <!-- slider-nav.// -->						
					</article> <!-- gallery-wrap .end// -->
				</aside>
				<div id="contents"></div>
				<aside class="col-sm-5">
					<form method="post" action="{{url('add_carrito')}}" enctype="multipart/form-data" id="form_prod" class="col-12">
						{{ csrf_field() }} 
					<article class="card-body p-5">
						<h3 class="title mb-3">{{$info_individual->producto_nombre}} <br><span style="font-size: 15px;margin-bottom: 30px;" class="badge">{{$info_individual->codigoInterno}}</span></h3>
							<p class="price-detail-wrap"> 
								<span class="price h3 "> 
									<span class="num">${{number_format($info_individual->precio,2)}}</span><span class="currency"> MXN </span>
								</span> 
								<!--<span>/per kg</span> -->
							</p> <!-- price-detail-wrap .// -->
							
							<dl class="item-property">
  								<dt></dt>
  									<dd>
  										<p>{{$info_individual->descripcion}}
										</p>
									</dd>
							</dl>						
						<hr>
			
						<div class="row">
												
							<div class="col-sm-12">
								<dl class="param param-inline">
						  			<dt align="center" >Selecciona tu talla </dt>
						  			<dd>
						  				<div id="tallas" align="center">
											@if($info_individual->txCh)
											<label class="btns" for="ech"><input type="radio" class="talls" name="talla" id="ech" value="ECH" style="display:none ;">ECH</label>
											@endif

											@if($info_individual->tCh)
											<label class="btns" for="ch"><input type="radio" class="talls" name="talla" id="ch" value="CH" style="display: none ;">CH</label>
											@endif

											@if($info_individual->tM)
											<label class="btns" for="m"><input type="radio" class="talls" name="talla" id="m" value="M" style="display:  none;">M</label>
											@endif

											@if($info_individual->tG)
											<label class="btns"  for="g"><input type="radio" class="talls" name="talla" id="g" value="G" style="display: none ;">G</label>
											@endif

											@if($info_individual->txG)
											<label class="btns" for="eg"><input type="radio" class="talls" name="talla" id="eg" value="EG" style="display: none ;">EG</label>
											@endif
										</div>
										<input type="hidden" name="producto" value="{{$info_individual->identifica_prod}}" style="display: none;">
										<input type="hidden" name="idusuario" value="{{Session::get('id_usuario')}}" style="display: none;">
										<input type="hidden" name="precio" value="{{$info_individual->precio}}" style="display: none;">
						  				<input type="hidden" name="nombre_prod" value="{{$info_individual->producto_nombre}}" style="display: none;">
						  			</dd>
						  			
						  			<!--<dd><p align="center"><a href="" id="modaltallas">Guia de tallas</a></p></dd>-->
						  			<dd><p align="center"><button type="button" class="btn" id="modaltallas" data-toggle="modal" data-target="#myModal">Guia de tallas</button></p></dd>
								</dl>  <!-- item-property .// -->
							</div> <!-- col.// -->

						</div> <!-- row.// -->
					
						<hr>
					
						<!--<a href="#" class="btn btn-lg btn-primary text-uppercase"> Buy now </a>
						<a href="#" class="btn btn-lg btn-outline-primary text-uppercase"> <i class="fas fa-shopping-cart"></i> Add to cart </a>-->
						<input type="submit" id="registro" value="Añadir a carrito" >
						<dl class="param param-feature">
  								<dt><b>+ Info</b></dt>	
  								<dd><b>+ <a href="{{url('guia','talla')}}">Cuidados y condiciones</a></b></dd>
							</dl> 
						</form>
					</article> <!-- card-body.// -->
				</aside> <!-- col.// -->
			</div> <!-- row.// -->
		</div> <!-- card.// -->
	</div>
	

<!--<dd><p align="center"><a href="" id="modaltallas">Guia de tallas</a></p></dd>-->

	<br><br>

	<div id="woman" style="text-align: center;">
		<div class="contenedor">
			
			<div class="row">
				@foreach($art_relacion as $individual)
				<h2>Productos Relacionados</h2>
					<div class="col-sm">
						<img src="{{asset('/img/producto.png')}} " alt="">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. .</p>
					</div>
				@endforeach

				<!--<div class="col-sm">
					<img src="{{asset('/img/producto.png')}} " alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. .</p>
				</div>
				<div class="col-sm">
					<img src="{{asset('/img/producto.png')}} " alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. .</p>
				</div>
				<div class="col-sm">
					<img src="{{asset('/img/producto.png')}} " alt="">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. .</p>
				</div>-->
			</div>
		</div>
	</div>	

		
	

@stop



