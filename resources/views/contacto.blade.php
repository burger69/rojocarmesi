@extends('base2')

@section('content')
	
	
		<div id="total">
			<div class="contenedor">
				<h1 class="accionar"><div id="tittleSesion">Contacto</div><div id="tittleRegistre">&nbsp;</div></h1>
			</div>
		</div>
		<br>
		<div class="cuerpoacerca">
			<div class="contenedor">
				<div class="row">
					<div class="col-lg-4" id="correos">
						<ul>
							<li><a href="mailto:servicioalcliente@rojocarmesimx.com">servicioalcliente@rojocarmesimx.com</a></li>
							<li><a href="mailto:pepe.pech@rojocarmesimx.com">pepe.pech@rojocarmesimx.com</a></li>
							<li><a href="mailto:pau.lomeli@rojocarmesimx.com">pau.lomeli@rojocarmesimx.com</a></li>
							<!--<li><a href="www.google.com">servicioalcliente@rojocarmesimx.com.mx</a></li>-->
						</ul>
					</div>
					<div class="col-lg-8">
						<div class="formulariocontacto">
							@if(isset($mensaje))
								{!!$mensaje!!}
							@endif
						<form method="post" action="{{url('helpsend')}}">	 
						{{ csrf_field() }}						
							<div class="wrap">
		                        <div class="mat-div">
		                            <label for="name" class="mat-label">Nombre</label>
		                            <input type="text" class="mat-input" name="name" id="name">
		                        </div>

		                        <div class="mat-div">
		                            <label for="mail" class="mat-label">Correo</label>
		                            <input type="text" class="mat-input" name="mail" id="mail">
		                        </div>

		                        <div class="mat-div">
		                            <label for="mensaje" class="mat-label">Mensaje:</label><br>
									<textarea name="mensaje" id="mensaje" class="form-control" cols="30" rows="10"></textarea>
		                        </div>

		                        <div class="mat-div">
									<input type="submit" id="registro2" class="btn btn-outline-danger" value="Enviar">
								</div>
                    		</div>
						</form>
						</div>

					</div>
				</div>
			</div>
		</div>
@stop