@extends('base')

@section('content')

	<div class="contenedor">
		<div id="men">
			<div class="row">
				<div class="col-lg-6" id="hombre">
					<h1 id="subtitulo">Elegante</h1>
					<img id="elegante" src="{{asset('/img/hombre2.png')}}" alt="">
				</div>
				<div class="col-lg-6" id="hombre">
					<h1 id="subtitulo1">Esenciales</h1>
					<img id="esencial" src="{{asset('/img/hombre1.png')}}" alt="">
				</div>
			</div>
		</div>
		
		<br><br>
		
		<div class="row">
			@foreach($productos as $producto)

	        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ContentGalerie">
	            <div class="product-grid6">
	                <div class="product-image6">
	                    <a href="{{url('vista_producto',$producto->identifica_prod)}}">
	                        <img class="pic-1" src="{{$producto->ruta_imagen}}">
	                    </a>
	                </div>
	                <div class="product-content">
	                    <h3 class="title"><a href="{{url('vista_producto',$producto->identifica_prod)}}">{{$producto->producto_nombre}}</a></h3>
	                    <div class="price">${{ number_format($producto->precio,2)}} MXN
	                        <!--<span>$14.00</span>-->
	                    </div>
	                </div>
	                <ul class="social">
	                    <li><a href="{{url('vista_producto',$producto->identifica_prod)}}" data-tip="Quick View"><i class="fa fa-search"></i></a></li>
	                    <li><a href="" data-tip="Add to Wishlist"><i class="fa fa-heart"></i></a></li>
	                    <li><a href="" data-tip="Add to Cart"><i class="fa fa-shopping-cart"></i></a></li>
	                </ul>
	            </div>
	        </div>
            @endforeach
            
        </div>
        	<nav aria-label="...">
	    		<ul class="pagination justify-content-center">
	         		{{$productos->links('vendor.pagination.bootstrap-4')}}
	     		</ul>
			</nav>
	</div>

@stop