<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Rojo Carmesí</title>

	<link rel="stylesheet" href="{{ asset('/plugins/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('/css/styles_prueba.css')}}">
	<link rel="stylesheet" href="{{asset('/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">

	<link rel="stylesheet" href="{{asset('/plugins/alertifyjs/css/alertify.min.css')}}">
	<link rel="stylesheet" href="{{asset('/plugins/alertifyjs/css/themes/default.min.css')}}">
	
	<link href=" {{asset('/css/styleadmin.css')}}" rel="stylesheet" type="text/css" media="all"/>
	<!--Google Fonts-->
	<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
	
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>
<body>
		<div id="total">
			<div class="contenedor">
				<nav class="navbar navbar-expand-lg navbar-light bg-light" id="menu_base2">
		  			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
		    			<span class="navbar-toggler-icon"></span>
		  			</button>
		  			<a class="navbar-brand visible-xs-block text-center" href="{{url('/')}}" id="rojito">
		  				<img src="{{asset('/img/logo2.png')}} " alt="Rojo Carmesí"></a>
				  	<div class="collapse navbar-collapse" id="navbarTogglerDemo01">		   		
				    	<ul class="navbar-nav mr-auto mt-5 mt-lg-0">
				      		<li id="oculto"><a href="#popup" class="popup-link"><img src="{{asset('/icons/menu_w.png')}} " alt=""></a></li>
							<li class="item"><a href="{{ url('hombre') }}">Hombre</a></li>
							<li class="item"><a href="{{url('mujer')}} ">Mujer</a></li>
							<li id="logo"><a href="{{url('/')}}"><img src="{{asset('/img/logo2.png')}}" alt="Rojo Carmesí"></a></li>
							@if(Session::get('nombre_usuario') == '')
								<li class="item"><a href="{{url('/login')}}"><i class="fa fa-user-o" aria-hidden="true"></i>Iniciar Sesion</a></li>
							@else								
								<li class="nav-item dropdown" id="user_data2">
        							<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          								{{Session::get('nombre_usuario')}}
        							</a>
        							<div class="dropdown-menu" aria-labelledby="navbarDropdown">
          								@if(Session::get('perfil') != 'ADMIN')
          								<a class="dropdown-item" href="{{url('miperfil')}}">Mi Perfil</a>
          								<a class="dropdown-item" href="{{url('logout')}}">Cerrar Sesión</a>
          								@elseif(Session::get('perfil') == 'ADMIN')
										<a class="dropdown-item" href="{{url('admin')}}">Administrar</a>
          								<a class="dropdown-item" href="{{url('logout')}}">Cerrar Sesión</a>
          								@endif
        							</div>
      							</li>

							@endif
							<li class="item"><a href="{{url('vista_carrito')}}"><span id="cantidad"> <strong>{{session::get('productos_carrito')}}</strong></span><img width="25px" height="25px" src="{{asset('/icons/shopping22.png')}}" alt=""></a></li>
				    	</ul>
				  	</div>
				</nav>
				


			</div>
		</div>

	
	<!--@yield('content')-->
	

	

<!--<script src="{{asset('/plugins/jquery/jquery-3.3.1.slim.js')}}"></script>-->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="{{asset('/plugins/poppers/popper.js-1.12.9/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<script src="{{asset('/plugins/alertifyjs/alertify.min.js')}}"></script>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>

<script src="{{asset('/js/funciones.js')}}"></script>
<script src="https://www.paypal.com/sdk/js?client-id=sb&currency=USD"></script>
<script>var url_global='{{url("/")}}';</script>

    <script>
        // Render the PayPal button into #paypal-button-container
        paypal.Buttons({
        	style: {
                color:  'blue',
                shape:  'pill',
                label:  'pay',
                height: 40
            },
            // Set up the transaction
            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: '{{\Session::get("neto_paypal")}}'
                        }
                    }]
                });
            },

            // Finalize the transaction
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    // Show a success message to the buyer
                    alert('Transaction completed by ' + details.payer.name.given_name + '!');
                });
            }


        }).render('#paypal-button-container');
    </script>
</body>
</html>