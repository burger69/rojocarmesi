@extends('base2')

@section('content')
    <div id="total">
        <div class="contenedor">
                <!--<h1>Registrarse</h1>-->
                <h1><div id="tittleRegistre2">Inicia Sesión/</div><div id="tittleSesion2">Registrarse</div></h1>
        </div>
    </div>

<div class="cuerpoacerca">
    <div class="contenedor">
        <div class="medio">
            <div class="row">
                <div class="col">
                                                                            

                    <form method="post" action="{{route('register')}}">
                        {{ csrf_field() }}  
                    <div class="wrap">
                        @if(isset($exito))
                            {!!$exito!!}
                            @unset($exito)
                        @endif

                        @if(\Session::has('exito'))
                            {!!\Session::get('exito')!!}                       
                        @endif

                        
                        <div class="mat-div">
                            @if ($errors->has('name'))
                                <div class="alert alert-danger success-alert" id='success-alert' role="alert">
                                    <buton type="button" class='close' data-dismiss="alert"> &times;</buton>
                                    <p>Comprueba el campo nombre</p>
                                </div>
                            @endif
                            <label for="name" class="mat-label">Nombre</label>
                            <input type="hidden" value="CLIENTE" name="tipo_user">
                            <input type="text" maxlength="60" class="mat-input" name="name" id="name" value="{{old('name')}}">
                        </div>
                      
                        <div class="mat-div">
                            <label for="appaterno" class="mat-label">Primer Apellido</label>
                            <input type="text" maxlength="40" class="mat-input" name="appaterno" id="appaterno" value="{{old('appaterno')}}">
                        </div>

                        <div class="mat-div">
                            <label for="apmaterno" class="mat-label">Segundo apellido</label>
                            <input type="text" maxlength="40" class="mat-input" name="apmaterno" id="apmaterno" value="{{old('apmaterno')}}">
                        </div>

                        <div class="mat-div">
                            @if ($errors->has('email'))
                                <div class="alert alert-danger success-alert" id='success-alert' role="alert">
                                    <buton type="button" class='close' data-dismiss="alert"> &times;</buton>
                                    <p>{{$errors->first('email') }}</p>
                                </div>
                            @endif                            
                            <label for="mailR" class="mat-label">Correo</label>
                            <input type="email" maxlength="45" class="mat-input" name="email" id="mailR" value="{{old('email')}}">
                        </div>

                        <div class="mat-div">
                            @if ($errors->has('mail2'))
                                <div class="alert alert-danger success-alert" id='success-alert' role="alert">
                                    <buton type="button" class='close' data-dismiss="alert"> &times;</buton>
                                    <p>Confirma correctamente el correo</p>
                                </div>
                            @endif
                            <label for="mail2" class="mat-label">Confirmar correo</label>
                            <input type="email" maxlength="45" class="mat-input" name="mail2" id="mail2" value="{{old('mail2')}}">
                        </div>

                        <div class="mat-div">
                            @if ($errors->has('password'))
                                <div class="alert alert-danger success-alert" id='success-alert' role="alert">
                                    <buton type="button" class='close' data-dismiss="alert"> &times;</buton>
                                    <p>La contraseña es obligatoria</p>
                                </div>
                            @endif 
                            <label for="password" class="mat-label">Contraseña</label>
                            <input type="password"  class="mat-input" name="password" id="password">
                        </div>

                        <div class="mat-div">
                            @if ($errors->has('password_confirmation'))
                                <div class="alert alert-danger success-alert" id='success-alert' role="alert">
                                    <buton type="button" class='close' data-dismiss="alert"> &times;</buton>
                                    <p>Confirma correctamente la contraseña</p>
                                </div>
                            @endif 
                            <label for="password2" class="mat-label">Confirmar contraseña</label>
                            <input type="password" class="mat-input" name="password_confirmation" id="password2">
                        </div>


                        <label for="promociones" class="mat-label"><input type="checkbox" class="mat-check" name="promociones" id="promociones">Deseo recibir ofertas y novedades</label>
                        
                        <button id="registro">Registrarse</button>

                        <div class="hey">Al hacer click en "Registrarse", aceptas nuestra <p><a href="#" id="privacidad">Politica de privacidad</a></p></div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@stop