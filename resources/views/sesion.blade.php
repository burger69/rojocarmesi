@extends('base2')


@section('content')
		<div id="total">
			<div class="contenedor">
				<h1><div id="tittleSesion">Inicia Sesión/</div><div id="tittleRegistre">Registrarse</div></h1>
			</div>
		</div>
		<div class="cuerpoacerca">
			<div class="contenedor">
				<div class="medio">
					<div class="row">
						<div class="col-lg-12 formulario">
							
							<form method="post" action="{{route('login')}}">
								{{ csrf_field() }} 
								<br><br>
								<div class="wrap">
									@include('errorUser')
			                     		@if(\Session::has('exito'))
			                       			{!!\Session::get('exito')!!}
			                       			@unset($mensaje)
                     					@endif
									<div class="mat-div">
			                            <label for="correo" class="mat-label">Correo</label>
			                            <input type="email" class="mat-input" name="email" id="email" value="{{old('email')}}">
                        			</div>
								<br>
                        			<div class="mat-div">
			                            <label for="password" class="mat-label">Contraseña</label>
			                            <input type="password" class="mat-input" name="password" id="password">
                        			</div>
									
									<div class="mat-div">
		                                <div class="checkbox">
		                                    <label class="mat-label">
		                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
		                                    </label>
		                                </div>
                            		</div>
                        		
								</div>

								<!--<br><br>
								<label for="correo">E-mail</label>
								<input type="text" name="correo" required id="correo" class="form-control" placeholder="E-mail"><br>
								
								<label for="password">Contraseña:</label>
								<input type="password" name="password" required id="password" class="form-control" placeholder="Contraseña">-->

								<br>
								<div class="sum" style="text-align: center;">
									<div class="row">
										<div class="col align-self-center">
											<input type="submit" id="send" class="form-control btn btn-outline-danger" value="Entrar" style="width:150px;">
											<p><a href="{{url('resetear')}}">¿Olvidaste tu contraseña?</a><br>
												<a href="{{route('register')}} ">
													Crea una cuenta
												</a>
											</p>
										</div>
									</div>
									
								</div>
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>

@stop