@extends('base')


@section('content')

		<!--<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>-->
		
		<!------ Include the above in your HEAD tag ---->
		@if (isset($msg))
			{!!$msg!!}
		@endif
		<div class="contenedor">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  	<ul class="carousel-indicators">
					    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	  				</ul>

				<div class="carousel-inner" role="listbox">
				    <div class="carousel-item active">
				      	<img class="d-block img-fluid" src="{{asset('/img/banner4.jpg')}}" alt="First slide">
				      	<div class="carousel-caption d-md-block">
	    					<h3>Descubre <br> lo nuevo <br> de esta <br> temporada</h3>
	  					</div>
				    </div>
				    <div class="carousel-item">

				      <img class="d-block img-fluid" src="{{asset('/img/banner4.jpg')}}" alt="Second slide">
				      <div class="carousel-caption d-md-block">
	    					<h3>Descubre <br> lo nuevo <br> de esta <br> temporada</h3>
	  					</div>
				    </div>
				    <div class="carousel-item">
				      <img class="d-block img-fluid" src="{{asset('/img/banner4.jpg')}}" alt="Third slide">
				      <div class="carousel-caption d-md-block">
	    					<h3>Descubre <br> lo nuevo <br> de esta <br> temporada</h3>
	  					</div>
				    </div>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
		</div>
		<br><br>
	

@stop