@extends('base')


@section('content')

	
			<div class="contenedor">
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  	<ul class="carousel-indicators">
					    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
					    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	  				</ul>

					<div class="carousel-inner" role="listbox">
					    <div class="carousel-item active">
					      	<img class="d-block img-fluid" src="{{asset('/img/catalog.png')}}" alt="First slide">
					      	<div class="carousel-caption d-md-block">
		    					<!--<h3>Hola</h3>
		    					<p>...</p>-->
		  					</div>
					    </div>

					    <div class="carousel-item">
					      <img class="d-block img-fluid" src="{{asset('/img/banner4.jpg')}}" alt="Second slide">
					    </div>

					    <div class="carousel-item">
					      <img class="d-block img-fluid" src="{{asset('/img/catalog.png')}}" alt="Third slide">
					    </div>
					</div>
					
					<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
					    <span class="sr-only">Previous</span>
					</a>
				  
				  	<a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
				    	<span class="sr-only">Next</span>
				  	</a>
				</div>
				
				<br><br>

				<div class="row">
					<!--@foreach($productos as $producto)
		            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ContentGalerie">
		                team-img --
		                <div class="team-block">
		                    <div class="team-img">
		                        <a href="{{url('vista_producto',$producto->identifica_prod)}}"><img src="{{asset($producto->ruta_imagen)}}" alt=""></a>
		                        <div class="team-content">
		                            <h4 class="text-white mb0">{{$producto->producto_nombre}}</h4>
		                            <p class="team-meta">${{ number_format($producto->precio,2)}} MXN</p>
		                        </div>
		                    </div>
		                </div>
		            </div>
		            @endforeach	  -->  

		            @foreach($productos as $producto)
			            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 ContentGalerie">
			                <!-- team-img -->
			                <div class="team-block" style="background-image: url('{{$producto->ruta_imagen}}')">
			                    <div class="team-img">
			                        <a href="{{url('vista_producto',$producto->identifica_prod)}}"><!--<img class="img-fluid" src="{{asset($producto->ruta_imagen)}}" alt="Producto">--></a>
			                        <div class="team-content">
			                            <a href="{{url('vista_producto',$producto->identifica_prod)}}"><h4 class="text-white mb0">{{$producto->producto_nombre}}</h4></a>
			                            <p class="team-meta">${{ number_format($producto->precio,2)}} MXN</p>
			                        </div>
			                    </div>
			                </div>
			            </div>
            		@endforeach        
		        </div>
	        	
	        	<nav aria-label="...">
		    		<ul class="pagination justify-content-center">
		         		{{$productos->links('vendor.pagination.bootstrap-4')}}
		     		</ul>
				</nav>
        	</div>

@stop





