<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Rojo Carmesí</title>

	<link rel="stylesheet" href="{{ asset('/plugins/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('/css/styles.css')}}">
	<link rel="stylesheet" href="{{asset('/plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<link href="{{asset('/css/styleadmin.css')}}" rel="stylesheet" type="text/css" media="all"/>
	<link rel="stylesheet" type="text/css" href="{{asset('/js/fancy/dist/jquery.fancybox.min.css')}}">
	
	<link rel="stylesheet" href="{{asset('/plugins/alertifyjs/css/alertify.min.css')}}">
	<link rel="stylesheet" href="{{asset('/plugins/alertifyjs/css/themes/default.min.css')}}">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>

<body>
	<div class="contenedor">
		<nav class="navbar navbar-expand-lg navbar-light bg-light" id="base_uno">
			
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
	   			<span class="navbar-toggler-icon"></span>
			</button>
	
			<a class="navbar-brand visible-xs-block text-center" href="{{url('/')}}" id="rojito">
				<img src="{{asset('/img/logo.png')}} " alt="Rojo Carmesí">
			</a>
			
		  	<div class="collapse navbar-collapse" id="navbarTogglerDemo01">		   		
		    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0">
		      		<li id="oculto"><a href="#popup" class="popup-link"><img src="{{asset('/icons/menu_2.png')}} " alt=""></a></li>
					<li class="item"><a href="{{ url('hombre') }}">Hombre</a></li>
					<li class="item"><a href="{{url('mujer')}} ">Mujer</a></li>
					<li id="logo"><a href="{{url('/')}}"><img src="{{asset('/img/logo.png')}}" alt="Rojo Carmesí"></a></li>
			
				@if(!Auth::check())
					<li class="item"><a href="{{url('/login')}}"><i class="fa fa-user fa-w"></i>Iniciar Sesion</a></li>
				@else
					
						<!--<li class="item"><a href="{{url('/login')}}"><i class="fa fa-user fa-w"></i>Iniciar Sesion</a></li>-->
					
					<li class="nav-item dropdown" id="user_data1">
    					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    						<img width="10px;" height="10px" style="margin-right: 15px;" src="{{asset('/icons/usuario.png')}} " alt="">
      							{{ Auth::user()->name }}	
    					</a>
    					
    					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
      						
      						@if(Auth::user()->hasRole('user'))
      							<a class="dropdown-item" href="{{url('miperfil')}}">Mi Perfil</a>
      							<a class="dropdown-item" href="{{route('logout')}}">Cerrar Sesión</a>
      						@elseif(Auth::user()->hasRole('manager'))
								<a class="dropdown-item" href="{{url('admin')}}">Administrar</a>
      							<a class="dropdown-item" href="{{route('logout')}}">Cerrar Sesión</a>
      						@endif
    					</div>
  					</li>
  				
				@endif	

					<li class="item">
						<a href="{{url('vista_carrito')}}">
							<span id="cantidad">
								<strong>@if(Session::has('productos_carrito')) {{ Session::get('productos_carrito')  }} @endif</strong>
							</span>
							<img width="25px" height="25px" src="{{asset('/icons/shopping.png')}}" alt="">
						</a>
					</li>
				</ul>
			</div>
		</nav>
	</div>
	<br>
	@yield('content')
	
	<br>
	<div id="semifoot">		
		<div class="contenedor">
			<div class="row">
				<div class="col" id="equis">
					<img src="{{asset('/img/equis-3.png')}}" alt="">
					<img src="{{asset('/img/equis-3.png')}}" alt="">
					<img src="{{asset('/img/equis-3.png')}}" alt="">
				</div>
			</div>
		</div>
	</div>
	
	<div id="prefoot">
		<div class="contenedor">
			<div class="row">
				<div class="col-lg-3 tittlefoot">
					<strong><a href="{{url('guia')}}"><h4>Guia de Compra</h4></a></strong>
						<ul>
							<li><a href="{{url('guia','talla')}}">Tallas y cuidados</a></li>
							<li><a href="{{url('guia','entrega')}}">Entregas</a></li>
							<li><a href="{{url('guia','cambios')}}">Cambios</a></li>
							<li><a href="{{url('guia','pagos')}}">Pagos</a></li>
						</ul>					
				</div>
				<div class="col-lg-3 tittlefoot">
					<strong><a href="{{ url('nosotros','nosotros') }}"><h4>Sobre <l>Rojo Carmesí</l></h4></a></strong>
					<ul>
						<li><a href="{{ url('nosotros','nosotros') }}">Quienes somos</a></li>
						<li><a href="#" id="privacidad">Privacidad</a></li>
						<!--<li><a href="{{url('contacto')}} ">Contacto</a></li>-->
					</ul>
				</div>
				<div class="col-lg-3 tittlefoot">
					<strong><a href="{{url('contacto')}} "><h4>Contacto</h4></a></strong>
				</div>
				<div class="col-lg-3">
						<h4>Siguenos en:</h4>
						<img src="{{asset('/icons/fb-black2.png')}}" alt="">
						<img src="{{asset('/icons/twitter.png')}}" alt="">
						<img src="{{asset('/icons/instagram.png')}}" alt="">
						<img src="{{asset('/icons/pinte.png')}}" alt="">
				</div>
			</div>
		</div>
	</div>

	<div id="targets">
		
	</div>
	

	<div class="modal-wrapper" id="popup">
	   	<div class="popup-contenedor">
	   	<a class="popup-cerrar" href="#">X</a><br><br>
	      <h2>Titulo de la Modal</h2>	      
	   	</div>
	</div>

	<div class="modal fade show" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Guia de tallas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="material-icons">X</i>
                    </button>
                    
                    
                </div>
                <div class="modal-body">
                   
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>

<!--<script src="{{asset('/plugins/jquery/jquery-3.3.1.slim.js')}}"></script>-->
<script src="{{asset('/plugins/jquery/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('/plugins/poppers/popper.js-1.12.9/dist/umd/popper.min.js')}}"></script>
<script src="{{asset('/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/jquery.zoom.js')}}"></script>
<script src="{{asset('/plugins/alertifyjs/alertify.min.js')}}"></script>
<script src="{{asset('/js/funciones.js')}}"></script>
<!--<script src="https://secure.mlstatic.com/sdk/javascript/v1/mercadopago.js"></script>-->
<script>var url_global='{{url("/")}}';</script>
<script>

            $('[data-fancybox="images"]').fancybox({
                buttons : [
                'share',
                'thumbs',
                'close'
                ]
            });
        </script>

    <script src="{{asset('/js/fancy/dist/jquery.fancybox.min.js')}}"></script>
	<script src="{{asset('/js/elevatezoom.js')}}">
		
		$(".zoom_01").elevateZoom();
	</script>


</body>
</html>