<div class="container">
	<div class="col-lg-12">		

		<div class="card">
            <div class="card-header">
                <strong><h5>Control de proveedores de Mensajería</h5></strong>
            </div>
            <div class="card-body card-block">
            	<form action="{{url('guarda_servicio')}}" id="form_services" method="post" enctype="multipart/form-data" class="form-horizontal">
            		<div class="row form-group">
                        <div class="col-12 col-lg-8">		                         
                            <input type="text" placeholder="Nombre Servicio" name="nombre_servicio" value="" id="nombre_servicio"  class="form-control">		                           
                        </div>
						<br><br>
						<div class="col-12 col-lg-4">
                            <input type="text" placeholder="Costo" name="costo_servicio" value="" id="costo_servicio" class="form-control">
                        </div>
                	</div>

                	<div class="row form-group">
                        <div class="col-12 col-lg-8">		                         
                            <input type="file" placeholder="Logo Servicio" name="logo_servicio" accept="image/*" value="" id="logo_servicio"  class="form-control">		                           
                        </div>
						<br><br>
						<!--<div class="col-12 col-lg-4">
                            <select name="estatus_servicio" id="estatus_servicio" class="form-control">
                            	<option value="%">Selecciona una opción</option>
                            	<option value="1">Activado</option>
                            	<option value="0">Desactivado</option>
                            </select>
                        </div>-->
                	</div>

                	<div class="row form-group">
                        <div class="col-12 col-lg-8">
                        </div>
						<br><br>
						<div class="col-12 col-lg-4" style="text-align: right;">
                            <input type="submit" value="Guardar" class="btn btn-warning">
                        </div>
                	</div>

            	</form>
            </div>
        </div>

	</div>
	<br>
	<div class="col-lg-12">
		<h5>Servicios:</h5><br>
			
			@php $i =0; @endphp
			@foreach($allservices as $servicio => $nombre)
				@if($i%3 == 0)
					<div class="row">
				@endif
				<div class="col">
					<div class="card">
			            <div class="card-header">
			                <strong><h5>{{$nombre['nombre_flete']}}</h5></strong>
			            </div>
			            <div class="card-body card-block">
			            	<div class="row">
				            	<div class="col">
				            		<img src="{{asset($nombre['ruta_logo'])}}" alt="" style="border: 1px solid;" >
				            	</div>
				            	
				            	<div class="col">			            		
					            	<ul style="margin-left:5px; list-style: none;">
					            		<li><b>Costo: </b> ${{number_format($nombre['costo'],2)}}</li>
					            		<!--<li><b>Estatus: </b></li>-->
					            		<!--<li><b></b>{{$nombre['nombre_flete']}}</li>-->
					            	</ul>
				            	</div>
			            	</div><!--fin de row-->
			            	<br>
				            	<a href="{{url('re',base64_encode($nombre['id']))}}" class="btn btn-danger">Eliminar</a>
				            	<!--<a href="" class="btn btn-warning">Desactivar</a>-->
			            </div>
        			</div>
				</div>
				
				@if($i%3 == 2)
					</div>
				@endif				
			@php $i++; @endphp
			@endforeach
	</div><!--Fin de col-lg-12-->
</div>