<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domicilio_sinregistro extends Model
{
    protected $table = 'domicilio_sinregistro';
    //protected $primaryKey = 'idDomicilio';
    public $timestamps = false;
}
