<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    //
     protected $table = 'domicilio';
     protected $primaryKey = 'idDomicilio';
     public $timestamps = false;

}
