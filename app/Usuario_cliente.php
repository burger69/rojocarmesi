<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario_cliente extends Model
{
    //
    protected $table = 'cliente';
    protected $primaryKey = 'idCliente';

    protected $fillable = [
    	'nombre', 'correo', 'contrasena','perfil'
 	];

 	protected $hidden = [
 		'contrasena', 'remember_token'
 	];
}
