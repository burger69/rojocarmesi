<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensajeria extends Model
{
    protected $table = 'fletes';
    protected $primaryKey = 'idflete';
    public $timestamps = false;
}
