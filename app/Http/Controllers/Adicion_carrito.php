<?php

namespace App\Http\Controllers;
Use Auth;
use App\Pedido;
use App\RelacionPedidoUser;
use App\Domicilio;
use App\Domicilio_sinregistro;
use App\TmpVenta;
use App\Productos;
use App\Mensajeria;
use App\Mail\GestorMail;
use MercadoPago;
//use MP;

use Illuminate\Http\Request;

class Adicion_carrito extends Controller
{
    function agregarcarrito(Request $request){
    	
    	if($request->session()->get('arregloCompra') == ""){
    		$arreglodecompra = array();
    		$arregloPedidos = array();
    	}
    	
        if($this->funcioPrueba($request)){	
    		if($request->session()->has('pedido_virtual') || \Session::get('pedido_virtual') != ""){
    			
    			$origen = \Session::get('pedido_virtual');
    			$index = count($origen);						
    			$arregloProducto['identi_trasaccion'] = $index;
    			$arregloProducto['identi_producto'] = $request->producto;
    			$arregloProducto['talla'] = $request->talla;
    			$arregloProducto['precio'] = $request->precio;
                $arregloProducto['precioUni'] = $request->precio;
    			$arregloProducto['ruta_img'] = $request->ruta_imagen;
    			$arregloProducto['nombre_producto'] = $request->nombre_prod;
                $arregloProducto['cantidad_prod'] = 1;
    			
    			$origen[] = $arregloProducto;

    			$request->session()->put('pedido_virtual',$origen);
    			$request->session()->put('productos_carrito',count($origen));			
    		}
    		else{			
    			
                $arregloProducto = array();
    			$arregloCarrito = array();

    			$arregloProducto['identi_trasaccion'] = 0;
    			$arregloProducto['identi_producto'] = $request->producto;
    			$arregloProducto['talla'] = $request->talla;
    			$arregloProducto['precio'] = $request->precio;
                $arregloProducto['precioUni'] = $request->precio;
    			$arregloProducto['ruta_img'] = $request->ruta_imagen;
    			$arregloProducto['nombre_producto'] = $request->nombre_prod;
                $arregloProducto['cantidad_prod'] = 1;
    			

    			$arregloCarrito[] = $arregloProducto;
    			$request->session()->put('pedido_virtual', $arregloCarrito);
    			$request->session()->put('productos_carrito',count($arregloCarrito));		
    		}
        }//tmp
    }

    /*
    Funcion que busca si existe el producto en el arreglo, si hay existencia incrementa el numero del producto
    */
    function funcioPrueba($info){
        
        $existeProd = true;

        $origen = \Session::get('pedido_virtual');

        if($info->session()->has('pedido_virtual')){
            foreach ($origen as $clave => $valor) {
                if($valor['identi_producto'] == $info->producto && $valor['talla'] == $info->talla){                
                    $indice = $valor['identi_trasaccion'];
                    $precio = $valor['precio']/$origen[$indice]['cantidad_prod'];             
                    $cantidaActual = $origen[$indice]['cantidad_prod'];                    
                    $talla = $origen[$indice]['talla'];
                    $cantidaActual = (int)$cantidaActual + 1;
                    $origen[$indice]['cantidad_prod'] = $cantidaActual;
                    $origen[$indice]['precio'] = $cantidaActual*$precio;
                    \Session::put('pedido_virtual',$origen);
                    $existeProd = false;       
                }    
            }
        }

        return $existeProd; 
    }


    function vistaCarrito(){
    	
    	$usuarioNull = false;
        $articulosCompra = \Session::get('pedido_virtual');
        //\Session::forget('pedido_virtual');
       //dd($articulosCompra);
        return view('vista_carrito.micarrito')->with('articulos_compra',$articulosCompra)->with('avisoUsuario',$usuarioNull)->with('fletes',"");
    	
    }

    function llenarDatos(){

        /*if(Auth::check())^{
            return redirect('login');
        }*/


        $infoBase = array('calle' => '', 'cruzamientos' => '',
                          'numero_int'=>'', 'numero_ext' => '', 
                          'cruzamientos' => '', 'colonia_fracc' => '', 
                          'ciudad' => '', 'estado' => '', 
                          'cp' => '','envio' => '');

        
        if(\Session::has('pedido_virtual')){
            
            $articulosCompra = \Session::get('pedido_virtual');

            $arregloInfoPago = \Session::get('pago_paq');
        
            $TotalProd = 0;        

            foreach ($articulosCompra as $key => $value) {        
                $TotalProd = $TotalProd+$value['precio'];
            }

            $infoBase['totalNeto'] = $infoBase['subtotal'] = $TotalProd;

            //SE COMENTA POR DESACTIVACION DE MENSAJERIA
            /*if(!isset($arregloInfoPago['totalNeto'])){
                
                $infoBase['paqueteria'] = "";
                $infoBase['costoPaque'] = 0;
                $infoBase['totalNeto'] = 0;
            }
            else{

                $infoBase['costoPaque'] = $arregloInfoPago['costoPaque'];
                $infoBase['totalNeto'] = $TotalProd+$arregloInfoPago['costoPaque'];
            }*/

            //SE COMENTA POR DESACTIVACION DE MENSAJERIA
            /*$allMensaje = Mensajeria::all()->toArray();
            foreach ($allMensaje as $llave => $valor) {
                    $allMensaje[$llave]['key'] = $this->codificacion($valor['id']);
            }*/
            //dd(Auth::User());
            if(Auth::User() == NULL){ //Si no hay un usuario logueado
                
                $usuarioNull = true;            
                return view('vista_carrito.datos_pago')->with('datosDomicilio',$infoBase);//->with('allservices', $allMensaje);
            }
            else{ //Si existe usuario verifico si tiene domicilio registrado.
                
                if(Domicilio::where('id',Auth::User()->id)->exists()){//utilizo id que es el que relaciona la tabla
                    
                    //$articulosCompra = \Session::get('pedido_virtual');
                    $datosUsuario = Auth::User();
                    $infoDomicilio = Domicilio::where('id',Auth::User()->id)->get()[0];
                    $infoBase['nombre_usuario'] = $datosUsuario->name;    
                    $infoBase['appaterno_usuario'] = $datosUsuario->appaterno;
                    $infoBase['apmaterno_usuario'] = $datosUsuario->apmaterno;
                    $infoBase['nombre_usuario'] = $datosUsuario->name;    
                    $infoBase['calle'] = $infoDomicilio->calle;
                    $infoBase['cruzamientos'] = $infoDomicilio->cruzamientos;
                    $infoBase['numero_int'] = $infoDomicilio->numero_int;
                    $infoBase['numero_ext'] = $infoDomicilio->numero_ext;
                    $infoBase['colonia_fracc'] = $infoDomicilio->colonia_fracc;
                    $infoBase['ciudad'] = $infoDomicilio->ciudad;
                    $infoBase['estado'] = $infoDomicilio->estado;
                    $infoBase['cp'] = $infoDomicilio->cp;
                    $infoBase['ciudad'] = $infoDomicilio->calle;
                    $infoBase['envio'] = 0;

                    return view('vista_carrito.datos_pago')->with('datosDomicilio',$infoBase);//->with('allservices', $allMensaje);
                }
                else{
                    
                    $aviso = "<script>alertify.error('Sin Información de envío');</script>";
                    return view('vista_carrito.datos_pago')->with('avisoSinDomicilio',$aviso)->with('datosDomicilio',$infoBase);
                }
            } 
        }
        else{
            return redirect('/');
        }

        //return view('vista_carrito.datos_pago');
    }

    function quitarProducto($index){
       //dd($index);
    	$articulosCompra = \Session::get('pedido_virtual');
		if(isset($articulosCompra[$index])){
            unset($articulosCompra[$index]);
        }
		\Session::put('pedido_virtual', $articulosCompra);
    	\Session::put('productos_carrito',count($articulosCompra));
		return $this->vistaCarrito();		
    }

    function finalizaPedido($request){
    	
    	if(floatval(str_replace(',',"", str_replace("$","",$request['source'])))  > 0){

	    	$objectPedido = new Pedido();
	    	$objectPedido->idcliente = \Session::get('id_usuario');
	    	$objectPedido->costoPedido = floatval(str_replace(',',"", str_replace("$","",$request['source'])));//$request->tota_neto;
	    	$objectPedido->estatusPedido = "ENPROCESO";
	    	$objectPedido->fecha_levantada = date("Y-m-d H:i:s");
	    	$objectPedido->save();

	    	$todos = $objectPedido->all();
	    	$ultimoRegistroPedido = $todos->last();

    		$miarreglo = \Session::get('pedido_virtual');                		
            //dd($miarreglo);
	    	foreach ($miarreglo as $registro) {	    	
	    		$objectRelacion = new RelacionPedidoUser();
	    		$objectRelacion->idPedido = $ultimoRegistroPedido->idpedido;
	    		$objectRelacion->idProducto = $registro['identi_producto'];
                $objectRelacion->talla = $registro['talla'];
                $objectRelacion->cantidad = $registro['cantidad_prod'];
	    		$objectRelacion->save();
	    	}    		

	    	if(\Session::get('id_usuario')==""){
	    	
	    		$objectSinRegistro = new Domicilio_sinregistro();
	    		$objectSinRegistro->idPedido = $ultimoRegistroPedido->idpedido;
	    		$objectSinRegistro->nombre_sin = $request['nombre_c'];
	    		$objectSinRegistro->calle = $request['calle_c'];
	    		$objectSinRegistro->cruzamientos = $request['cruzamiento_c'];
	    		$objectSinRegistro->numero_ext = $request['noexterior_c'];
	    		$objectSinRegistro->numero_int = $request['nointerior_c'];
	    		$objectSinRegistro->colonia_fracc = $request['colonia_c'];
	    		$objectSinRegistro->ciudad = $request['ciudad_c'];
	    		$objectSinRegistro->estado = $request['estado_c'];
	    		$objectSinRegistro->cp = $request['cp_c'];
	    		$objectSinRegistro->save();
	    	}
    		
    		\Session::forget('pedido_virtual');
    		\Session::forget('productos_carrito');
            \Session::forget('pago_paq');    		            
    	}
    	else{
    		dd("No tienes productos");
    	}
    }

    public function getCreatePreference(Request $request){       
        

        \Session::put('micompra',$request->all());
        //dd($request->all());
        \Session::put('precioTotal', floatval($request->tota_neto));
        //dd(\Session::get('pedido_virtual'));
        
        if($this->validarCompra(\Session::get('pedido_virtual'),str_replace("$", "",$request->source))){
            
            //MercadoPago\SDK::setAccessToken("ENV_ACCESS_TOKEN");
            MercadoPago\SDK::setClientId("2387250165183315");
            MercadoPago\SDK::setClientSecret("RJVk6zW9WX7u5vnaS6l8SgIAtgbkwbXD");
            # Create a preference object
            $preference = new MercadoPago\Preference();

            $preference->back_urls = array(
                "success" => asset('')."succesBuy/".$this->compraTmpSave($request->all()),
                "failure" => asset('')."failBuy",
                "pending" => asset('')."denegBuy"
            );

            $preference->payment_methods = array(
                'installments' => 1
            );
            
            $preference->auto_return = "approved";
            # Create an item object
            $item = new MercadoPago\Item();
            $item->id = "1";
            $item->title = "Productos Rojo Carmesí";
            $item->quantity = 1;
            $item->currency_id = "MXN";
            $item->unit_price = floatval(str_replace(',',"", str_replace("$","",$request->source)));
            $item->picture_url = $request->ruta_imagenMail;
            # Create a payer object
            $payer = new MercadoPago\Payer();
            $payer->name = $request->nombre_c;
            $payer->email = $request->correo_c;
            # Setting preference properties
            $preference->items = array($item);
            $preference->payer = $payer;
            # Save and posting preference
            $preference->save();
            //dd($preference);
            echo $preference->sandbox_init_point;



            /*$preferenceData = [
                'items' => [
                    [
                        'id' => 12
                        ,
                        'category_id' => 'Ropa',
                        'title' => 'Compra de Rojo Carmesí',
                        'description' => 'Ultimo paso de compra por mercado pago',
                        //'picture_url' => 'http://d243u7pon29hni.cloudfront.net/images/products/iphone-6-dorado-128-gb-red-4g-8-mpx-1256254%20(1)_m.png',
                        'quantity' => 1,
                        'currency_id' => 'MX',
                        'unit_price' => floatval(str_replace(',',"", str_replace("$","",$request->source))),
                        'picture_url' => $request->ruta_imagenMail
                    ]
                ],

                'back_urls' => [
                    'success' => 'http://localhost/rojocarmesi/public/succesBuy/'.$this->compraTmpSave($request->all()),
                    "failure" => "http://localhost/rojocarmesi/public/failBuy",
                    "pending" => "http://localhost/rojocarmesi/public/denegBuy"
                ],

                'payer' =>[
                	'name'=> $request->nombre_c,
        			//'surname' => $request->ruta_imagenMail,
                	'email' => $request->correo_c
                ],

                

                'payment_methods' => [
    				'installments' => 1,
    				/*'excluded_payment_methods' => [
    					[
    						'id' => 'visa'
    					]
    				],*jpj
                ],

                'auto_return' => 'approved'
            ];*/

            //JPJ$preference = MP::create_preference($preferenceData);

            //$modalPago = view('vista_carrito.modalPago')->with('pago', $preference['response']['sandbox_init_point']);
            //JPJreturn  $preference['response']['sandbox_init_point'];//dd($preference);
        }
        else{
            return asset('')."failBuy";
        }

        

        //$modalPago = view('vista_carrito.modalPago')->with('pago', );
       // return $modalPago;// dd($preference);
    }

    function compraTmpSave($data){
       
       //dd($data);
       $vtaTmp = new TmpVenta();

       $vtaTmp->tota_neto = floatval(str_replace(',',"", str_replace("$", "", $data['source'])));
       $vtaTmp->nombre_c = $data['nombre_c'];
       $vtaTmp->correo_c = $data['correo_c'];
       $vtaTmp->celular_c = $data['celular_c'];
       $vtaTmp->calle_c = $data['calle_c'];
       $vtaTmp->nointerior_c = $data['nointerior_c'];
       $vtaTmp->noexterior_c = $data['noexterior_c'];
       $vtaTmp->cruzamiento_c = $data['cruzamiento_c'];
       $vtaTmp->colonia_c = $data['colonia_c'];
       $vtaTmp->ciudad_c = $data['ciudad_c'];
       $vtaTmp->estado_c = $data['estado_c'];
       $vtaTmp->cp_c = $data['cp_c'];
       $vtaTmp->img_prod = \Session::get('pedido_virtual')[0]['ruta_img'];//$data['ruta_imagenMail'];
       $vtaTmp->costo_envio = \Session::get('pago_paq')['costoPaque'];
       $vtaTmp->acompletado_r = 0;
       
       $vtaTmp->save();

       $IdVtaTmp = TmpVenta::where('correo_c',$data['correo_c'])->select('id')->orderby('id','desc')->limit(1)->get();
       
       return $IdVtaTmp[0]->id;
    }

    function compraSatisfactoria($idcompra){

        $compra = TmpVenta::where('id',$idcompra)->get()->toArray();
       
        if($compra){
           foreach ($compra as $key2 => $valor) {
                $io = $valor;
            }
            $io['subject'] = "¡Compra Exitosa!";

            \Mail::to($io['correo_c'])->send(new GestorMail($io));

            if(\Session::has('pedido_virtual') && \Session::get('pedido_virtual') != ''){
                $tablaTmp = TmpVenta::find($idcompra);
                $tablaTmp->acompletado_r = 1;
                $tablaTmp->save();
                $this->finalizaPedido(\Session::get('micompra'));
                return view('pedidos.ventaSucces');
            }
            else{
                return redirect('/');
            }
        }
        
    }

    function compraDenegada(){
        
        if(\Session::has('pedido_virtual') && \Session::get('pedido_virtual') != ''){
            $this->finalizaPedido(\Session::get('micompra'));
            return view('pedidos.ventaDepossito');
        }
        else{
            return view('inicio');
        }

    }

    function falloCompra(){
        
        return view('pedidos.ventaRechaza');
        return $this->vistaCarrito();
    }

    function validarCompra($contenido,$precio){
        

        $precio = floatval(str_replace(',',"", $precio));
        $ValidarTotal = 0;
        
        foreach ($contenido as $clave => $valor) {
            
            $PrecioP = Productos::where('idProducto',$valor['identi_producto'])->select('precio')->get();
            $ValidarTotal = $ValidarTotal+$PrecioP[0]->precio*$valor['cantidad_prod'];
            $valor2[] = $valor['identi_producto'];
        }

        if(\Session::has('pago_paq')){
          $ValidarTotal = Mensajeria::where('id', \Session::get('pago_paq')['paqueteria'])->get()[0]->costo+$ValidarTotal;
        }
       
        if($precio == $ValidarTotal){
            return true;
        }
        else{
            return false;
        }
    }

    function updateCarrito(Request $request){


        $origen = \Session::get('pedido_virtual');

        $precio = $origen[$request->posicioProd]['precio'];//\Session::get('pedido_virtual')[$request->posicioProd]['precio'];
        $cantidadActual = $origen[$request->posicioProd]['cantidad_prod'];//\Session::get('pedido_virtual')[$request->posicioProd]['cantidad_prod'];
        
        if($request->valorCant >0){
            $precioUnitario = $precio/$cantidadActual;
            $nuevoPrecioTotal = $precioUnitario*$request->valorCant;
            $origen[$request->posicioProd]['precioUni'] = $precioUnitario;
            $origen[$request->posicioProd]['precio'] = $nuevoPrecioTotal;
            $origen[$request->posicioProd]['cantidad_prod'] = $request->valorCant;
            \Session::put('pedido_virtual',$origen);
        }
    }

    //Desactivamos esta funcion por que ya el precio de la mensajeria vendra incluida en los precios del producto
    /*function updateCosto(Request $request){

        
        $precioLimpio = str_replace("$", "",$request->pcio);//Precio de la paqueteria

        $clave = $this->decodifica($request->cve); //Clave del registro en la BD

        if(Mensajeria::where("id",$clave)->exists()){
            if(Mensajeria::where("id",$clave)->get()[0]['costo'] == $precioLimpio){
               
               $Virtual = \Session::get('pedido_virtual');
               
               $arregloInfoPago = array();
               $totalCompra = 0;
               
               foreach ($Virtual as $item => $valor) {
                   $totalCompra = $totalCompra + $valor['precio'];
               }
               
               $arregloInfoPago['paqueteria'] =  Mensajeria::where("id",$clave)->get()[0]['id'];
               $arregloInfoPago['subtotal'] = $totalCompra;
               $arregloInfoPago['costoPaque'] = Mensajeria::where("id",$clave)->get()[0]['costo'];
               $totalCompra = $totalCompra + Mensajeria::where("id",$clave)->get()[0]['costo'];
               $arregloInfoPago['totalNeto'] = $totalCompra;

               \Session::put('pago_paq',$arregloInfoPago);
               \Session::forget('neto_paypal');
               \Session::put('neto_paypal',$arregloInfoPago['totalNeto']);

                $jsonDevuelve = array(
                    'paquete' => $arregloInfoPago['costoPaque'],
                    'neto' => $arregloInfoPago['totalNeto'] 
                );
            }
            echo json_encode($jsonDevuelve, JSON_FORCE_OBJECT);
        }
    }*/

    function codificacion($valor){

        $valor = $valor."_pos";

        return base64_encode(base64_encode($valor));

    }

    function decodifica($valor){

        $valor = base64_decode(base64_decode($valor));
        $valorEncontrar = "_pos";
        $pos   = strripos($valor, $valorEncontrar);
        $valor = substr($valor,0, $pos);

        return $valor;

    }


}