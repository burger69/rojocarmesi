<?php


namespace App\Http\Controllers;
use App\Pedido;
use App\Domicilio;
use App\Domicilio_sinregistro;
use App\RelacionPedidoUser;
use App\Imagenes_prod;
use App\Productos;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class VistaPedido extends Controller
{
    function verPedidos(){

        if (!\Auth::check()) {
            return redirect('/');
        }

    	$arrayConsultas = array();

    	$pedidosActivos = Pedido::where('estatusPedido','ENPROCESO')->get();

    	if(count($pedidosActivos) > 0){
    		$arrayConsultas['Activos'] = $pedidosActivos;
    	}

    	$pedidosCancelados = Pedido::where('estatusPedido','CANCELADO')->get();
    	

    	if(count($pedidosCancelados) > 0){
    		$arrayConsultas['Cancelados'] = $pedidosCancelados;
    	}

    	$pedidosEnviados = Pedido::where('estatusPedido','ENVIADO')->get();
    	

    	if(count($pedidosEnviados) > 0){
    		$arrayConsultas['Enviados'] = $pedidosEnviados;
    	}


    	//dd($arrayConsultas);
    	
    	$form_men = view('pedidos.viewPedidos')->with('proceso',$arrayConsultas);
    	return view('panel.panelcontrol')->with('vista',$form_men);
    	
    }

    function vistaDetallePedido($idpedido){
    	//dd($idpedido);

    	/*$infoOneProduct = \App\Productos::leftJoin("rel_producto_imagen","productos.idproducto","=","rel_producto_imagen.idproducto")
                 ->leftJoin('imagenes',"rel_producto_imagen.idimagen","=","imagenes.idimagen")
                        ->where('productos.idproducto','=',$id)->select("productos.idProducto as identifica_prod","productos.nombre as producto_nombre","productos.descripcion","productos.precio","productos.activo","productos.seccion",
                            "imagenes.idImagen as identificador_img",
                            "imagenes.nombre as imagen_nombre",
                            "imagenes.descripcion as imagen_desc",
                            "imagenes.rutaImagen as ruta_imagen")->get();*/
        if (!\Auth::check()) {
            return redirect('/');
        }                            

        $infoOnePedido = \App\Pedido::leftJoin("domicilio","pedido.idcliente","=","domicilio.idcliente")
        		->leftJoin('cliente', 'pedido.idcliente','=','cliente.idcliente')
                 ->leftJoin('rel_pedido_producto',"pedido.idpedido","=","rel_pedido_producto.idpedido")
                        ->where('pedido.idpedido','=',$idpedido)->get();

        $Comprobar = $infoOnePedido[0]->idCliente;
        //dd($infoOnePedido);

        if($Comprobar == NULL){
            $infoOnePedido = \App\Pedido::leftJoin("domicilio_sinregistro as incogni","pedido.idpedido","=","incogni.idpedido")
                ->leftJoin('cliente', 'pedido.idcliente','=','cliente.idcliente')
                 ->leftJoin('rel_pedido_producto',"pedido.idpedido","=","rel_pedido_producto.idpedido")
                        ->where('pedido.idpedido','=',$idpedido)->get();
        }
        //dd($infoOnePedido);
        $tallas = array();
        foreach ($infoOnePedido as $key ) {
        	$tallas['tallas'][] = $key->talla;              
            $tallas['cantidad'][] = $key->cantidad;              
        }

        //dd($tallas);

        $datosProd = $this->traeProductos($infoOnePedido);
        //dd($datosProd);
        $form_men = view('pedidos.detallePedido')->with('data', $infoOnePedido)->with('dataPedido',$datosProd)->with('tallasPedido',$tallas);
        
        return view('panel.panelcontrol')->with('vista',$form_men);
    }

    function traeProductos($arrayPedido){
    	//dd($arrayPedido);

        if (!\Auth::check()) {
            return redirect('/');
        }

    	$rutasImagen2 = array();
    	$i=0;
    	foreach($arrayPedido as $key) {
    		$rutasImagen[] = \App\Productos::leftJoin("rel_producto_imagen","productos.idproducto","=","rel_producto_imagen.idproducto")
                 ->leftJoin('imagenes',"rel_producto_imagen.idimagen","=","imagenes.idimagen")
                        ->where('productos.idproducto','=',$key->idProducto)->select("productos.idProducto as identifica_prod","productos.nombre as producto_nombre","productos.descripcion","productos.precio","productos.activo","productos.seccion",
                            "imagenes.idImagen as identificador_img",
                            "imagenes.nombre as imagen_nombre",
                            "imagenes.descripcion as imagen_desc",
                            "imagenes.rutaImagen as ruta_imagen")->limit(1)->get();       
            
            //$rutasImagen->put('talla', $key->talla);
            //$rutasImagen2[] = $rutasImagen;
    	}

    	//dd($rutasImagen2);


    	return $rutasImagen;
    }

    function cambio_status(Request $request){        

        $cambiar_pedido = Pedido::find($request->identifica_pedido);
        $cambiar_pedido->estatusPedido = $request->estatusPedido;
        $cambiar_pedido->save();

        return $this->verPedidos();
        //dd($cambiar_pedido);
    }
}
