<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mensajeria;

class ControllerFlete extends Controller
{
    //
    function index(){
		
        if (!\Session::has('perfil')) {
            return redirect('/');
        }

		$vista = view('controlfletes.principal_fletes')->with('allservices', Mensajeria::all()->toArray());
		return view('panel.panelcontrol')->with('vista',$vista);
    }

    function guardaServicio(Request $request){
    	//dd($request->all());

    	$imgLogo = $request->file('logo_servicio');

        //dd($extension);

    	$mensaje = new Mensajeria();
        $todos = $mensaje->all();
        if(count($todos) == 0){
           $nombreImg = "mensajeria1";
        }
        else{
            $numer = $todos->last()->toArray()['id']+1;
            $nombreImg = "mensajeria".$numer;
        }

    	$mensaje->nombre_flete = $request->nombre_servicio;
    	$mensaje->costo = $request->costo_servicio;
    	
    	if($imgLogo != ""){

            $extension = $imgLogo->getClientOriginalExtension();
            $nombreImg = $nombreImg.".".$extension;

    		$caracteres = array(" ","-","&","!","#","$","%","/","(",")","=","'","?","¿","¡","*","+","~","}","]","`","ñ","Ñ","{","[","^",":",",",
                ";","|","°","¬");
            $replace = '';
            
            $nombreLimpio = str_replace($caracteres, $replace, $nombreImg);
            //dd($nombreLimpio);
            $tildes=array('á','é','í','ó','ú');
            $vocales=array('a','e','i','o','u');
            $nombreLimpio = str_replace($tildes, $vocales, $nombreImg);
            $path = public_path()."/serviciosmensajeria/";  //Segun seccion cre el directorio establecido en donde se guardara las imgs
            $imgLogo->move($path,$nombreLimpio);
    		$mensaje->ruta_logo = "/serviciosmensajeria/".$nombreLimpio;//$request->nombre_servicio;
    	}
    	
    	//$mensaje->activo = 1;//$request->estatus_servicio;
    	$mensaje->save();
        return $this->index();
    }

    function removeFlete($id){
        
        $idEliminar = base64_decode($id);

        \File::delete(public_path().Mensajeria::where('id',$idEliminar)->get()[0]->ruta_logo);
        Mensajeria::where('id',$idEliminar)->delete();

        return $this->index();
        //dd($idEliminar);
    }
}
