<?php

namespace App\Http\Controllers;
use App\Productos;
use App\Imagenes_prod;
use App\Rel_Producto_imagen;
use App\Http\Controllers\Image;

use Illuminate\Http\UploadedFile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Productos_1 extends Controller
{

    /*public function __construct()
    {
       $this->middleware('auth');
    }*/

    function vistaaltaProducto(){

        $producto = new Productos();
    	$todos = $producto->all();

    	//$form_men = view('products.formulario_prod');
    	$lista_p = view('products.lista_prod')->with('data',$todos);

    	$form_men = view('products.formulario_prod')->with('conten',$lista_p);


    	return view('panel.panelcontrol')->with('vista',$form_men);
    	//dd($request);
    }

    public function alta_prod(Request $request){
    	
        //if (!\Auth::check()) {
        //  return redirect('/');
        //}

        $request->user()->authorizeRoles(['employee', 'manager']);
        //$request->user()->authorizeRoles(['user', 'admin']);
        // Recupero la imagen
        /*
            desc_prod
            codigoInterno
            precio_prod
            seccion_prod
    
        */
        $validaciones = array(
                        'name_prod' => 'required|min: 10',
                        'desc_prod' => 'required|min:30|max:255',
                        'precio_prod' => 'required',
                        'imagen' => 'required',
                        'imagen.*' => 'mimes:jpeg,png,jpg,gif,sv|max:2048',//'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                     );

        $mensajes = array(
                    'min' => 'El texto que esta ingresando es muy corto, intente con otro',
                    'desc_prod.min' => 'Ingresa una descripcion de al menos :min caracteres',
                    'desc_prod.max' => 'El texto sobrepasa al tamaño permitido de :max',
                    'imagen.mimes' => 'La imagen no corresponde a un formato permitido',
                    'imagen.*.max' => 'La imagen excede el tamaño permitido, no exceda los :size Kbs',
                    );
        

        $validation = $this->validate($request,$validaciones,$mensajes);


    	$file = $request->file('imagen');

    	$producto = new Productos();

    	//Guardo los datos del producto
    	$producto->nombre = $request->name_prod; 
    	$producto->descripcion = $request->desc_prod;
        $producto->codigoInterno = $request->codigoInterno;
        
    	$producto->precio = $request->precio_prod;
    	$producto->seccion = $request->seccion_prod;

        $producto->txCh = ($request->txCh) ? 1 : 0;
        $producto->tCh = ($request->tCh) ? 1 :0;
        $producto->tM = ($request->tM) ? 1 :0;
        $producto->tG = ($request->tG) ? 1 : 0;
        $producto->txG = ($request->txG) ? 1 :0 ;

        $producto->activo = ($request->estatus_prod) ? 1 :0 ;


    	
    	$producto->save();
    	//termina el guardado del producto

    	
    	//tomo el ultimo registro ingresado
        $todos = $producto->all();
        $ultimoProd = $todos->last();

        //obtengo e numero de imagenes guardadas
        $ImgnNo = Rel_Producto_imagen::where('idProducto',$ultimoProd)->get()->count();
        $activaPortada = 1;

        foreach ($file as $unoauno) {
            //$nombre = $unoauno->getClientOriginalName(); //Consigo la ruta temporal de la imagen
            //Armo el nombre de la imagen
            $extension = $unoauno->getClientOriginalExtension();
            $ImgnNo = $ImgnNo+1;
            $nombre = $request->name_prod.$ImgnNo.".".$extension;

            $caracteres = array(" ","-","&","!","#","$","%","/","(",")","=","'","?","¿","¡","*","+","~","}","]","`","ñ","Ñ","{","[","^",":",",",
                ";","|","°","¬");
            $replace = '';
            
            $nombreLimpio = str_replace($caracteres, $replace, $nombre);

            $tildes=array('á','é','í','ó','ú');
            $vocales=array('a','e','i','o','u');
            
            $nombreLimpio = str_replace($tildes, $vocales, $nombreLimpio);

            
            //***** ruta donde guardare la imagen *******/

            //slash invertido para windows
            //$path = realpath(base_path().'/../public_html')."/catalogos/"; //Directorio HOSTING
            $path = $pathWindows = realpath('')."\\catalogos\\";
                
            //slash normal linux
            //$pathLinux = realpath(base_path().'/../public_html')."/catalogos/men/"; 

            /***Fin Ruta para guardar imagenes**/                        
            //dd(asset());
            if($request->seccion_prod == 'HOMBRE'){                               
                //Segun seccion creo el directorio establecido en donde se guardara las imgs
                $path = $path."men\\";
                $unoauno->move($path,$nombreLimpio);
                $ruta_imagen = "catalogos/men/".$nombreLimpio;
            }
            elseif ($request->seccion_prod == 'MUJER') {                
                $path = $path."woman\\";
                $unoauno->move($path,$nombreLimpio);
                $ruta_imagen = "catalogos/woman/".$nombreLimpio;
            }
        	

            //Guardo Informacion de la imagen
        	$tableImagen = new Imagenes_prod();
        	$tableImagen->nombre = $nombreLimpio;
        	$tableImagen->rutaImagen = $ruta_imagen;
            $tableImagen->activo = $activaPortada;
        	$tableImagen->save();

        	//Obtengo el id del ultimo registro ingreso
            $todosImagen = $tableImagen->all();
        	$ultimoImagen = $todosImagen->last();

        	//Guardo el pivote
            $pivote = new Rel_Producto_imagen();
        	//dd($ultimoProd->idProducto." ".$ultimoImagen->idImagen);
        	$pivote->idProducto = $ultimoProd->idProducto;
        	$pivote->idImagen = $ultimoImagen->idImagen;
        	$pivote->save();

            $activaPortada = 0;
        }

    	
        $lista_p = view('products.lista_prod')->with('data',$todos); //envio la variable $todos ya que contiene todo los productos
    	$form_men = view('products.formulario_prod')->with('conten',$lista_p);
    	
        $existo = "<div class='alert alert-success' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>Producto registrado</div>";
        \Session::flash('exito',$existo);

    	return view('panel.panelcontrol')->with('vista',$form_men);
    }

    function update_prod(Request $request){
        

        if (!\Auth::check()) {
            return redirect('/');
        }
        // Recupero la imagen

        $validaciones = array(
                        'name_prod' => 'required|min: 10',
                        'desc_prod' => 'required|min:30|max:255',
                        'precio_prod' => 'numeric',
                        'imagen.*' => 'mimes:jpeg,png,jpg,gif,sv|max:2048',//'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                     );

        $mensajes = array(
                    'min' => 'El texto que esta ingresando es muy corto, intente con otro',
                    'desc_prod.min' => 'Ingresa una descripcion de al menos :min caracteres',
                    'desc_prod.max' => 'El texto sobrepasa al tamaño permitido de :max',
                    'precio_prod' => 'Ingrese solamente numeros en este campo',
                    'imagen.mimes' => 'La imagen no corresponde a un formato permitido',
                    'imagen.*.size' => 'La imagen excede el tamaño permitido, no exceda los :size Kbs',
                    );

        $validation = $this->validate($request,$validaciones,$mensajes);

        $file = $request->file('imagen');

       // dd($file);
        
        $producto =  Productos::find($request->idregistro);//->first();
        //dd($producto);
        //Guardo los datos del producto
        $producto->nombre = $request->name_prod; 
        $producto->descripcion = $request->desc_prod;
        $producto->codigoInterno = $request->codigoInterno;
        $producto->precio = $request->precio_prod;
        $producto->seccion = $request->seccion_prod;
        
        $producto->txCh = ($request->txCh) ? 1 : 0;
        $producto->tCh = ($request->tCh) ? 1 :0;
        $producto->tM = ($request->tM) ? 1 :0;
        $producto->tG = ($request->tG) ? 1 : 0;
        $producto->txG = ($request->txG) ? 1 :0 ;

        $producto->activo = ($request->estatus_prod) ? 1 :0 ;

        $producto->save();

        //termina el guardado del producto
        
        //tomo el ultimo registro ingresado
        $todos = $producto->all();
        
        $ultimoProd = $request->idregistro;// $todos->last();
        $ImgnNo = Rel_Producto_imagen::where('idProducto',$ultimoProd)->get()->count();
        //dd(Rel_Producto_imagen::where('idProducto',$ultimoProd)->get()->count());

        if(count($file)>=1){
            foreach ($file as $unoauno) {
                $nombre = $unoauno->getClientOriginalName(); //Consigo la ruta temporal de la imagen
                $extension = $unoauno->getClientOriginalExtension();
                $ImgnNo = $ImgnNo+1;
                $nombre = $request->name_prod.$ImgnNo.".".$extension;
                //dd($nombre);
                //dd(filesize($unoauno)/10240);

                $caracteres = array(" ","-","&","!","#","$","%","/","(",")","=","'","?","¿","¡","*","+","~","}","]","`","ñ","Ñ","{","[","^",":",",",
                    ";","|","°","¬");
                $replace = '';
                
                $nombreLimpio = str_replace($caracteres, $replace, $nombre);

                $tildes=array('á','é','í','ó','ú');
                $vocales=array('a','e','i','o','u');
                $nombreLimpio = str_replace($tildes, $vocales, $nombreLimpio);
                //dd(asset(''));
                if($request->seccion_prod == 'HOMBRE'){
                    $path = realpath(base_path().'/../public_html')/*public_path()*/."/catalogos/men/";  //Segun seccion cre el directorio establecido en donde se guardara las imgs
                    //dd(realpath(base_path().'/../public_html'));
                    $unoauno->move($path,$nombreLimpio);
                    $ruta_imagen = "catalogos/men/".$nombreLimpio;//$path;
                    
                    //dd($path);
                }
                elseif ($request->seccion_prod == 'MUJER') {
                    $path = realpath(base_path().'/../public_html')."/catalogos/woman/";
                    $unoauno->move($path,$nombreLimpio);
                    $ruta_imagen = "catalogos/woman/".$nombreLimpio;//$path;
                }
                

                //Guardo Informacion de la imagen
                $tableImagen = new Imagenes_prod();
                $tableImagen->nombre = $nombreLimpio;
                $tableImagen->rutaImagen = $ruta_imagen;
                $tableImagen->activo = 0;
                $tableImagen->save();

                //Obtengo el id del ultimo registro ingreso
                $todosImagen = $tableImagen->all();
                $ultimoImagen = $todosImagen->last();

                //Guardo el pivote
                $pivote = new Rel_Producto_imagen();
                //dd($ultimoProd->idProducto." ".$ultimoImagen->idImagen);
                $pivote->idProducto = $ultimoProd;//$ultimoProd->idProducto;
                $pivote->idImagen = $ultimoImagen->idImagen;
                $pivote->save();
            }
        }

        
        $lista_p = view('products.lista_prod')->with('data',$todos); //envio la variable $todos ya que contiene todo los productos
        $form_men = view('products.formulario_prod')->with('conten',$lista_p);
        $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>Producto actualizado</div>";
        \Session::flash('exito',$existo);
        return view('panel.panelcontrol')->with('vista',$form_men);
    }


    function detalle_prod($id){

        if (!\Auth::check()) {
            return redirect('/');
        }
        //$infoOneProduct = \App\Productos::where('idProducto',$id)->get();

        $infoOneProduct = \App\Productos::leftJoin("rel_producto_imagen","productos.idproducto","=","rel_producto_imagen.idproducto")
                 ->leftJoin('imagenes',"rel_producto_imagen.idimagen","=","imagenes.idimagen")
                        ->where('productos.idproducto','=',$id)->select("productos.idProducto as identifica_prod","productos.nombre as producto_nombre","productos.descripcion","productos.precio","productos.activo","productos.seccion","productos.txCh","productos.tCh","productos.tM","productos.tG","productos.txG", "productos.codigoInterno",
                            "imagenes.idImagen as identificador_img",
                            "imagenes.nombre as imagen_nombre",
                            "imagenes.descripcion as imagen_desc",
                            "imagenes.rutaImagen as ruta_imagen")->get();
       

        $dta = [];
        
        foreach($infoOneProduct as $d){
            array_push($dta, $d);
        }
           
        $ruta = array();
        $identi = array();

        if(count($infoOneProduct) >= 1){
            foreach ($dta as $campo) {
                if($campo->imagen_nombre != ""){
                    if($campo->seccion == "HOMBRE"){
                       $section = "men";
                    }
                    elseif ($campo->seccion == "MUJER") {
                        $section = "woman";
                    }
                    
                    $ruta['srcImg'] = "catalogos\\".$section."\\".$campo->imagen_nombre;
                    $ruta['IdImg'] = $campo->identificador_img;
                    $identi[] = $ruta;
                }
            }
            //echo count($ruta)%2;
            //print_r($ruta);
            /*if($campo->ruta_imagen != ""){
                if(count($ruta)%2 != 0){
                    array_push($ruta, "img/default.png");
                }

                if( count($identi)%2 != 0){
                    array_push($identi, "");
                }
            }*/
        }
        
        //dd($identi);
        //dd($dta[0]->producto_nombre);

        $content = view('products.vista_images')->with('ruta_img',$ruta)->with('idImagen',$identi)->with("idProd",$id);
        $form_men = view('products.update_prod')->with('oneproduct',$dta)->with("idProd",$id)->with('conten',$content);
        return view('panel.panelcontrol')->with('vista',$form_men);
        //Session::get('perfil') 
    }

    function deleteimg($idimagen,$idproducto){

        if (!\Auth::check()) {
            return redirect('/');
        }

        if(Imagenes_prod::where('idImagen',$idimagen)->exists()){
            $rutaImagen = public_path(explode('/',Imagenes_prod::where('idImagen',$idimagen)->get()[0]->rutaImagen)[0].'\\'.explode('/',Imagenes_prod::where('idImagen',$idimagen)->get()[0]->rutaImagen)[1])."\\".explode('/',Imagenes_prod::where('idImagen',$idimagen)->get()[0]->rutaImagen)[2];

            if(\File::exists($rutaImagen)){
                \File::delete($rutaImagen);

            }//else{
               // dd('El archivo no existe.');
            //}

            Imagenes_prod::where('idImagen',$idimagen)->delete();
            Rel_Producto_imagen::where('idImagen',$idimagen)->where('idProducto',$idproducto)->delete();
        }
        
        return $this->detalle_prod($idproducto);
    }

    function productosPrincipal(){

        $todosProd = Productos::leftJoin("rel_producto_imagen","productos.idproducto","=","rel_producto_imagen.idproducto")
                 ->leftJoin('imagenes',"rel_producto_imagen.idimagen","=","imagenes.idimagen")
                        ->where('imagenes.activo','=',1)->where('productos.seccion','=','MUJER')->select("productos.idProducto as identifica_prod","productos.nombre as producto_nombre","productos.descripcion","productos.precio","productos.activo","productos.seccion",
                            "imagenes.idImagen as identificador_img",
                            "imagenes.nombre as imagen_nombre",
                            "imagenes.descripcion as imagen_desc",
                            "imagenes.rutaImagen as ruta_imagen")->paginate(6);//get();
        //dd($todosProd);



        return view('mujer')->with('productos',$todosProd);
    }

    function productosmen(){

        $todosProd = Productos::leftJoin("rel_producto_imagen","productos.idproducto","=","rel_producto_imagen.idproducto")
                 ->leftJoin('imagenes',"rel_producto_imagen.idimagen","=","imagenes.idimagen")
                        ->where('imagenes.activo','=',1)->where('productos.seccion','=','HOMBRE')->select("productos.idProducto as identifica_prod","productos.nombre as producto_nombre","productos.descripcion","productos.precio","productos.activo","productos.seccion",
                            "imagenes.idImagen as identificador_img",
                            "imagenes.nombre as imagen_nombre",
                            "imagenes.descripcion as imagen_desc",
                            "imagenes.rutaImagen as ruta_imagen")->paginate(6);//get();                            

        return view('hombre-mujer')->with('productos',$todosProd);
    }

    function detalle_producto($idproducto){


        if(Productos::where('idProducto',$idproducto)->exists()){
            $todosProd = Productos::leftJoin("rel_producto_imagen","productos.idProducto","=","rel_producto_imagen.idProducto")
                     ->leftJoin('imagenes',"rel_producto_imagen.idImagen","=","imagenes.idImagen")
                            ->where('productos.idProducto','=',$idproducto)->orderBy('imagenes.activo', 'desc')->select("productos.idProducto as identifica_prod","productos.nombre as producto_nombre","productos.descripcion","productos.precio","productos.activo","productos.seccion","productos.txCh","productos.tCh","productos.tM","productos.tG","productos.txG","productos.codigoInterno",
                                "imagenes.idImagen as identificador_img",
                                "imagenes.activo as portada_img",
                                "imagenes.nombre as imagen_nombre",
                                "imagenes.descripcion as imagen_desc",
                                "imagenes.rutaImagen as ruta_imagen")->get();

            foreach ($todosProd as $key => $value) {
             //  echo $value->producto_nombre;
            }

            //dd($todosProd);



            $prodRelacion = Productos::leftJoin("rel_producto_imagen","productos.idProducto","=","rel_producto_imagen.idProducto")
                     ->leftJoin('imagenes',"rel_producto_imagen.idImagen","=","imagenes.idImagen")
                            ->where('productos.idProducto','!=',$idproducto)->where('productos.nombre', 'like', '%' . $value->producto_nombre . '%')->orderBy('imagenes.activo', 'asc')->select("productos.idProducto as identifica_prod","productos.nombre as producto_nombre","productos.descripcion","productos.precio","productos.activo","productos.seccion","productos.txCh","productos.tCh","productos.tM","productos.tG","productos.txG","productos.codigoInterno",
                                "imagenes.idImagen as identificador_img",
                                "imagenes.activo as portada_img",
                                "imagenes.nombre as imagen_nombre",
                                "imagenes.descripcion as imagen_desc",
                                "imagenes.rutaImagen as ruta_imagen")->limit(3)->get();
            //dd($prodRelacion);

            return view('detallev2')->with('info_producto',$todosProd)->with('art_relacion',$prodRelacion);
        }
        else{
            return redirect('/');
        }
    }

    function deleteitem($idproducto){

        
        Productos::where('idProducto',$idproducto)->delete();
        $imagenes = Rel_Producto_imagen::where('idProducto',$idproducto)->get();
        
        //dd($imagenes);
        foreach ($imagenes as $campo => $valor ) {
            
            $rutaImagen = public_path(explode('/',Imagenes_prod::where('idImagen',$valor->IdImagen)->get()[0]->rutaImagen)[0].'\\'.explode('/',Imagenes_prod::where('idImagen',$valor->IdImagen)->get()[0]->rutaImagen)[1])."\\".explode('/',Imagenes_prod::where('idImagen',$valor->IdImagen)->get()[0]->rutaImagen)[2];

            if(\File::exists($rutaImagen)){
                \File::delete($rutaImagen);
            }

            Imagenes_prod::where('idImagen',$valor->IdImagen)->delete();            
        }

        Rel_Producto_imagen::where('idProducto',$idproducto)->delete();

        return $this->vistaaltaProducto();
        //dd($idproducto);
    }
}
