<?php

namespace App\Http\Controllers;
use App\Usuario_cliente;
use App\Domicilio;
use App\User;
use App\Domicilio_sinregistro;
use App\Http\Requests\Validations;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Auth;

class ControlRegistro extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('registro');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());

        $datos = new Usuario_cliente(); //Creo el objeto del modelo
       
        //dd($request->all());
        \Session::flash('todos',$request->all());
        
        if(!$datos->where('correo',$request->mail2)->exists()){ //consulto si esta registrado el usuario?       
            if($request->mailR == $request->mail2){//Comparacion de Correos
                if($request->password == $request->password2){// Comparacion de Passwords
                    if($this->verificarPass($request->password) == 0){                        
                    

                        $datos->nombre = $request->name;
                        $datos->appaterno = $request->appaterno;
                        $datos->apmaterno = $request->apmaterno;
                        $datos->correo = $request->mail2;
                        $datos->contrasena = bcrypt($request->password);
                        $datos->activo = 1;
                        $datos->perfil = "CLIENTE";
                        $datos->save();
                        //echo "hey";
                        $existo = "<div class='alert alert-success' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>Usuario registrado exitosamente! <br> Inicia Sesion</div>";
                        \Session::flash('exito',$existo);
                        return view('sesion')->with('exito', $existo);
                    }
                    else{
                        switch ($this->verificarPass($request->password)) {
                            case 1:
                                $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>La contraseña debe ser mayor a 8 caracteres</div>";
                                break;
                            
                            case 2:
                                $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>La contraseña deber menor o igual a 15 caracteres</div>";
                                break;
                            case 3:
                                $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>La contraseña debe tener al menos una letra minúscula</div>";
                                break;
                            case 4:
                                $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>La contraseña debe tener al menos una letra mayúscula</div>";
                                break;
                            case 5:
                                $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>La contraseña debe tener al menos un caracter numérico</div>";
                                break;
                        }

                        \Session::flash('exito',$existo);
                        return back()->withInput();
                    }
                }
                else{
                     $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>La contraseña no son las mismas</div>";
                    \Session::flash('exito',$existo);
                    return back()->withInput();
                    //return redirect('registro')->withInput(request(['email']));
                }
            }
            else{
                   $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>El correo no es el mismo</div>";
                    \Session::flash('exito',$existo);
                    //return redirect('registro');
                    return back()->withInput();
            }  
        }
        else{
            //echo $request->mail2;
            //echo "holi";
            $existo = "<div class='alert alert-danger' id='success-alert'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>¡El Correo Proporcionado ya existe!</div>";
            \Session::flash('exito',$existo);
            //return view('registro')->with('exito', $existo);
            //return redirect('registro');
            return back()->withInput();
        }

        
    }

    public function updatePerfil(Request $request){
       
        //dd($request->all());
        
        if(!Auth::check()){
            return redirect('login');
        }
        //dd(User::where('email',$request->mailR)->exists());
        //if(User::where('email',$request->mailR)->exists()){
        if(isset($request->email)){
            \Session::flash('msje','<div class="alert alert-danger success-alert">No puedes cambiar el correo de este modo.</div>');
            return redirect('miperfil');
        }
        
        //$this->validator($request->all())->validate();

        $objectUsario = new User();
        $informacion = $objectUsario->find(\Auth::user()->id);
        
        if($informacion){
            $informacion->name = $request->name;
            $informacion->appaterno = $request->appaterno;
            $informacion->apmaterno = $request->apmaterno;
            $informacion->celular = $request->celular;

            if(!Domicilio::where('id',Auth::user()->id)->exists()){
                
                $objectDomicilio = new Domicilio();
                $objectDomicilio->id = Auth::user()->id;
                $objectDomicilio->calle = $request->calle_c;                
                $objectDomicilio->cruzamientos = $request->cruzamiento_c;
                $objectDomicilio->numero_ext = $request->noexterior_c;
                $objectDomicilio->numero_int = $request->nointerior_c;
                $objectDomicilio->colonia_fracc = $request->colonia_c;
                $objectDomicilio->ciudad = $request->ciudad_c;
                $objectDomicilio->estado = $request->estado_c;
                $objectDomicilio->cp = $request->cp_c;
                $objectDomicilio->save();

            }
            else{
                //dd("no encontre");
                $UpdateDomicilio = Domicilio::where('id',\Auth::user()->id)->get();
                
                $objectDomicilio = Domicilio::find($UpdateDomicilio[0]->idDomicilio);
                $objectDomicilio->id = Auth::user()->id;
                $objectDomicilio->calle = $request->calle_c;
                $objectDomicilio->cruzamientos = $request->cruzamiento_c;
                $objectDomicilio->numero_ext = $request->noexterior_c;
                $objectDomicilio->numero_int = $request->nointerior_c;
                $objectDomicilio->colonia_fracc = $request->colonia_c;
                $objectDomicilio->ciudad = $request->ciudad_c;
                $objectDomicilio->estado = $request->estado_c;
                $objectDomicilio->cp = $request->cp_c;
                $objectDomicilio->save();
               // dd($UpdateDomicilio);

            }

            $informacion->save();
            \Session::flash('msje','<div class="alert alert-success success-alert">Se actualizaron tus datos.</div>');
        }

            //dd($objectUsario->find(\Session::get('id_usuario')));
        //}
        
        return redirect('miperfil');
    }

    public function uploadMail(Request $request){
        
        $this->validator($request->all())->validate();
        
        dd($request->all());
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            //'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            //'mail2' => 'required|string|email|max:255|same:email',
            //'password' => 'required|string|min:8|confirmed',            
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verificarPass($password)
    {
        $state = "";

        if(strlen($password) < 6){
            $error_clave = "La contraseña debe tener al menos 6 caracteres";
            return 1;
        }
        
        if(strlen($password) > 15){
            $error_clave = "La contraseña no puede tener más de 16 caracteres";
            return 2;
        }
       
        if (!preg_match('`[a-z]`',$password)){        
            return 3;
        }
   
        if (!preg_match('`[A-Z]`',$password)){
            return 4;
        }
        
        if (!preg_match('`[0-9]`',$password)){        
            return 5;
        }
        
        $error_clave = "";
        return 0;

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         echo "holi";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        echo "update=";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reset_password(Request $request){
        //
        //dd($request->email);
        if(Usuario_cliente::where('correo',$request->email)){
            dd('aqui hago creo token para reestablecer contraseña');
        }
        else{
            return back()->withInput();
        }
        echo "holi";
    }
}
