<?php

namespace App\Http\Controllers;
use App\Usuario_cliente;
use App\Domicilio;
use App\Domicilio_sinregistro;
//use App\Imagenes_prod;
//use App\Rel_Producto_imagen;
use App\Http\Controllers\Image;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Mail\TemplateContacto;


class UsuariosController extends Controller
{
    //

    function clientes_activos(){

        if (!\Auth::check()) {
            return redirect('/');
        }

        $usuariosActivos = Usuario_cliente::where('activo',1)->get();

       // dd($usuariosActivos);
        
        /*$lista_p = view('products.lista_prod')->with('data',$todos);
        $form_men = view('products.formulario_prod')->with('conten',$lista_p);
        return view('vistaadmin')->with('vista',$form_men);*/



        $lista = view("usuarios_cliente.lista_usuarios")->with("data",$usuariosActivos);
        $form_men = view('usuarios_cliente.formulario_usuarios')->with('conten',$lista);
        return view('panel.panelcontrol')->with('vista',$form_men);
    }


    function alta_usuario(Request $request){

        if (!\Auth::check()) {
            return redirect('/');
        }
        
        if(!Usuario_cliente::where('correo',$request->mail2)->exists()){ //consulto si esta registrado el usuario?       
            $datos = new Usuario_cliente;
            $datos->nombre = $request->name;
            $datos->appaterno = $request->appaterno;
            $datos->apmaterno = $request->apmaterno;
            $datos->correo = $request->mail2;
            $datos->contrasena = bcrypt($request->password);
            $datos->activo = 1;
            $datos->perfil = $request->perfil_alta;
            $datos->save();

            $existo = "<div class='alert alert-success'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>Usuario registrado exitosamente!</div>";
            
            //dd($existo);
            $usuariosActivos = $datos->all();
            $lista = view("usuarios_cliente.lista_usuarios")->with("data",$usuariosActivos);
            $form_men = view('usuarios_cliente.formulario_usuarios')->with('conten',$lista)->with('mensaje',$existo);
            return view('panel.panelcontrol')->with('vista',$form_men);
        }
        else{
            //echo $request->mail2;
            //echo "holi";
            //dd("Existe");
            $datos = new Usuario_cliente;
            $usuariosActivos = $datos->all();
            $existo = "<div class='alert alert-danger'><buton type='button' class='close' data-dismiss='alert'> &times;</buton>¡El Correo proporcionado ya existe, por favor capture otro!</div>";
            
            $lista = view("usuarios_cliente.lista_usuarios")->with("data",$usuariosActivos);
            $form_men = view('usuarios_cliente.formulario_usuarios')->with('conten',$lista)->with('mensaje',$existo);
            return view('vistaadmin')->with('vista',$form_men);
            //dd($existo);
            //return view('registro')->with('exito', $existo);
        }
    }

     public function vistaPerfil_panel($idusuario){
        
        if (!\Auth::check()) {
            return redirect('/');
        }

        foreach ($this->datosPerfil($idusuario) as $usuario){}
                
        $form_men = view('usuarios_cliente.perfil_control')->with("id_usuario",$usuario);
        return view('panel.panelcontrol')->with('vista',$form_men);               
        
    }

    public function datosPerfil($idCliente){

        if (!\Auth::check()) {
            return redirect('/');
        }

        $pefilUsuario = Usuario_cliente::leftJoin('domicilio','cliente.idCliente','=','domicilio.idCliente')->where('cliente.idCliente','=',$idCliente)->select('cliente.idCliente as identi_cliente','cliente.nombre','cliente.appaterno','cliente.apmaterno','cliente.celular','cliente.correo','domicilio.calle','domicilio.cruzamientos','domicilio.numero_ext','domicilio.numero_int','domicilio.colonia_fracc','domicilio.ciudad','domicilio.estado','domicilio.cp')->limit(1)->get();

        
        return $pefilUsuario;
        //exit();
    }

    function eliminar_perfil($idCliente){

        if (!\Auth::check()) {
            return redirect('/');
        }
    
        $noAdmis = count(Usuario_cliente::where('perfil','ADMIN')->get());

        if($noAdmis > 1){

            if(Usuario_cliente::where('idCliente',$idCliente)->exists() ){
                Usuario_cliente::where('idCliente',$idCliente)->delete();

                if(Domicilio::where('idCliente',$idCliente)->exists()  && $idCliente != 1){
                    Domicilio::where('idCliente',$idCliente)->delete();
                    return redirect('all_user');
                }
            }            
        }

         return $this->clientes_activos();
    }

    function envioAyuda(Request $request){

        if (!\Auth::check()) {
            return redirect('/');
        }

        $io = $request->all();
        $io['subject'] = "Servicio al cliente";

        \Mail::to('servicioalcliente@rojocarmesimx.com')->send(new TemplateContacto($io));
        
        $mensaje = '<div class="wrap" id="success-alert"><div class="alert alert-danger">Tu mensaje ha sido enviado con exito.</div></div>';
        return view('contacto')->with('mensaje',$mensaje);
    }
}
