<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class Direccionador extends Controller
{
    public function index(){
    	return view('inicio');
    }

    public function hombre(){
    	return view('hombre-mujer');
    }

    public function mujer(){
    	//return view('mujer');
        return view('auth.passwords.reset')->with(['token' => '', 'email' => '']);
    }

    public function detalle(){
    	return view('detalle');
    }


     public function nosotros(){
    	return view('nosotros');
    }

    public function guia(){
    	return view('guia');
    }

    public function contacto(){
    	return view('contacto');
    }

    public function registro(){
    	return view('registro');
    }

    public function sesion(){
    	return view('sesion');
    }

    public function resetPass(){
        return view('resetPassword');
    }

    public function inspiracion(){
    	return view('inspiracion');
    }

    public function vistaperfil(){
        return view('usuarios_cliente.miPerfil');
    }

    public function cargaChangeUpload(){

        if(!Auth::check()){
            redirect('/');
        }

        return view('changemail');
    }

}
