<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RelacionPedidoUser extends Model
{
     protected $table = 'rel_pedido_producto';

     public $timestamps = false;
}
