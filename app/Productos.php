<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    //
     protected $table = 'productos';

     protected $primaryKey = 'idProducto';

     /*function imag(){
     	return $this->belongsToMany('\App\Imagenes_prod', 'Rel_Producto_imagen')->withPivot('idProducto');
     }*/
}
